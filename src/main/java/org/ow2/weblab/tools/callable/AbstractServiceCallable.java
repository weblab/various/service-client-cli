/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.callable;

import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Just the abstract class that wraps callers and provide a sleep method
 *
 * @author ymombrun
 * @param <T>
 *            The type of object to be returned by the callable
 * @date 2012-02-15
 */
public abstract class AbstractServiceCallable<T> implements Callable<T> {


	/**
	 * Time to wait before call
	 */
	protected final long tempo;


	/**
	 * The logger used inside
	 */
	protected final Log logger;


	/**
	 * The exception that may have occurred during the process.
	 */
	protected Exception exception;


	/**
	 * @param tempo
	 *            The number of millisecond to wait
	 */
	public AbstractServiceCallable(final long tempo) {
		super();
		this.tempo = tempo;
		this.logger = LogFactory.getLog(this.getClass());
	}



	/**
	 * Waits tempo milliseconds
	 */
	protected void sleep() {
		Thread.yield();
		try {
			Thread.sleep(this.tempo);
		} catch (final InterruptedException ie) {
			this.logger.warn("Error while waiting.", ie);
		}
	}


	/**
	 * @return The exception that occurred or null
	 */
	public Exception getException() {
		return this.exception;
	}

}
