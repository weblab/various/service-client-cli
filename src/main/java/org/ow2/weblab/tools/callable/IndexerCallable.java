/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.callable;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;


/**
 * This class is an implementation of Callable to be used when testing Indexers.
 *
 * @author ymombrun
 * @date 2012-02-15
 */
public final class IndexerCallable extends AbstractServiceCallable<IndexReturn> {


	/**
	 * The indexer to be called
	 */
	private final Indexer indexer;


	/**
	 * The index args to be used
	 */
	private final IndexArgs args;


	public IndexerCallable(final long tempo, final Resource resource, final String usageContext, final Indexer indexer) {
		super(tempo);
		this.indexer = indexer;
		this.args = new IndexArgs();
		this.args.setResource(resource);
		this.args.setUsageContext(usageContext);
		this.logger.debug("IndexerCallable created for input resource '" + this.args.getResource().getUri() + "'.");
	}


	/**
	 * Call the indexer after sleeping tempo milliseconds
	 */
	@Override
	public IndexReturn call() throws Exception {
		this.sleep();
		this.logger.info("Calling index for input resource '" + this.args.getResource().getUri() + "'.");
		final IndexReturn indexRet;
		try {
			indexRet = this.indexer.index(this.args);
		} catch (final Exception e) {
			this.logger.error("An Exception occurs when indexing " + this.args.getResource().getUri() + "'.", e);
			this.exception = e;
			throw e;
		}
		this.logger.info("Response received for input resource + '" + this.args.getResource().getUri() + "'.");
		return indexRet;
	}


	public Resource getResource() {
		return this.args.getResource();
	}

}
