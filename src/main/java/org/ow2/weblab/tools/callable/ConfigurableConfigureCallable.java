/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.callable;

import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.services.Configurable;
import org.ow2.weblab.core.services.configurable.ConfigureArgs;
import org.ow2.weblab.core.services.configurable.ConfigureReturn;


/**
 * This class is an implementation of Callable to be used when testing method configure from Configurable.
 *
 * @author ymombrun
 * @date 2013-04-29
 */
public class ConfigurableConfigureCallable extends AbstractServiceCallable<ConfigureReturn> {


	/**
	 * The configurable to be called
	 */
	private final Configurable configurable;


	/**
	 * The configurable args to be used
	 */
	private final ConfigureArgs args;


	/**
	 * @param tempo
	 *            The temporisation to wait before calling the service
	 * @param pok
	 *            The <code>PieceOfKnowledge</code> to be put in the <code>ConfigureArgs</code>
	 * @param usageContext
	 *            The usage context to be put in the <code>ConfigureArgs</code>
	 * @param configurable
	 *            The <code>Configurable</code> to be called.
	 */
	public ConfigurableConfigureCallable(final long tempo, final PieceOfKnowledge pok, final String usageContext, final Configurable configurable) {
		super(tempo);
		this.configurable = configurable;
		this.args = new ConfigureArgs();
		this.args.setConfiguration(pok);
		this.args.setUsageContext(usageContext);
		this.logger.debug("ConfigurableConfigureCallable created for configuration '" + this.args.getConfiguration().getUri() + "' + and context "
				+ this.args.getUsageContext() + "'.");
	}


	/**
	 * Call the configurable after sleeping tempo milliseconds
	 */
	@Override
	public ConfigureReturn call() throws Exception {
		this.sleep();
		this.logger.info("Calling configure for configuration '" + this.args.getConfiguration().getUri() + "' + and context " + this.args.getUsageContext()
				+ "'.");
		final ConfigureReturn configureReturn;
		try {
			configureReturn = this.configurable.configure(this.args);
		} catch (final Exception e) {
			this.logger.error(
					"An Exception occured when configuring with pok '" + this.args.getConfiguration().getUri() + "' + and context "
							+ this.args.getUsageContext() + "'.", e);
			this.exception = e;
			throw e;
		}
		this.logger.info("Response received for configuration '" + this.args.getConfiguration().getUri() + "' + and context " + this.args.getUsageContext()
				+ "'.");
		return configureReturn;
	}


	public PieceOfKnowledge getPieceOfKnowledge() {
		return this.args.getConfiguration();
	}


}
