/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.callable;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.QueueManager;
import org.ow2.weblab.core.services.queuemanager.NextResourceArgs;


/**
 * This class is an implementation of Callable to be used when testing QueueManagers.
 *
 * @author ymombrun
 * @date 2013-04-29
 */
public final class QueueManagerCallable extends AbstractServiceCallable<Resource> {


	/**
	 * The queueManager to be called
	 */
	private final QueueManager queueManager;


	/**
	 * The next resource args to be used
	 */
	private final NextResourceArgs args;


	/**
	 * @param tempo
	 *            The temporisation to wait before calling the service
	 * @param usageContext
	 *            The usage context to be put in the <code>ProcessArgs</code>
	 * @param queueManager
	 *            The <code>QueueManager</code> to be called.
	 */
	public QueueManagerCallable(final long tempo, final String usageContext, final QueueManager queueManager) {
		super(tempo);
		this.queueManager = queueManager;
		this.args = new NextResourceArgs();
		this.args.setUsageContext(usageContext);
		this.logger.debug("QueueManagerCallable created for context '" + this.args.getUsageContext() + "'.");
	}


	/**
	 * Call the queueManager after sleeping tempo milliseconds
	 */
	@Override
	public Resource call() throws Exception {
		this.sleep();
		this.logger.info("Calling next resource method for context '" + this.args.getUsageContext() + "'.");
		final Resource res;
		try {
			res = this.queueManager.nextResource(this.args).getResource();
		} catch (final Exception e) {
			this.logger.error("An Exception occurs when calling next resource for context '" + this.args.getUsageContext() + "'.", e);
			this.exception = e;
			throw e;
		}
		this.logger.info("Response received for context '" + this.args.getUsageContext() + "'. New resource URI is '" + res.getUri() + "'.");
		return res;
	}


	public String getUsageContext() {
		return this.args.getUsageContext();
	}

}
