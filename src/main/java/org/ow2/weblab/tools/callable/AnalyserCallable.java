/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.callable;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;


/**
 * This class is an implementation of Callable to be used when testing Analysers.
 *
 * @author ymombrun
 * @date 2012-02-15
 */
public final class AnalyserCallable extends AbstractServiceCallable<Resource> {


	/**
	 * The analyser to be called
	 */
	private final Analyser analyser;


	/**
	 * The process args to be used
	 */
	private final ProcessArgs args;


	/**
	 * @param tempo
	 *            The temporisation to wait before calling the service
	 * @param resource
	 *            The resource to be put in the <code>ProcessArgs</code>
	 * @param usageContext
	 *            The usage context to be put in the <code>ProcessArgs</code>
	 * @param analyser
	 *            The <code>Analyser</code> to be called.
	 */
	public AnalyserCallable(final long tempo, final Resource resource, final String usageContext, final Analyser analyser) {
		super(tempo);
		this.analyser = analyser;
		this.args = new ProcessArgs();
		this.args.setResource(resource);
		this.args.setUsageContext(usageContext);
		this.logger.debug("AnalyserCallable created for '" + this.args.getResource().getUri() + "'.");
	}


	/**
	 * Call the analyser after sleeping tempo milliseconds
	 */
	@Override
	public Resource call() throws Exception {
		this.sleep();
		this.logger.info("Calling process method for input resource '" + this.args.getResource().getUri() + "'.");
		final Resource res;
		try {
			res = this.analyser.process(this.args).getResource();
		} catch (final Exception e) {
			this.logger.error("An Exception occurs when processing resource '" + this.args.getResource().getUri() + "'.", e);
			this.exception = e;
			throw e;
		}
		this.logger.info("Response received for resource '" + this.args.getResource().getUri() + "' new URI is '" + res.getUri() + "'.");
		return res;
	}


	public String getUsageContext() {
		return this.args.getUsageContext();
	}


	public Resource getResource() {
		return this.args.getResource();
	}

}
