/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import org.apache.commons.cli.ParseException;



/**
 * Created as a workaround to the fact ParseException has no constructor that sets the init cause.
 *
 * @author ymombrun
 * @date 2012-07-13
 */
public class CustomParseException extends ParseException {


	/**
	 *
	 */
	private static final long serialVersionUID = 100L;


	/**
	 * Construct a new <code>ParseException</code> with the specified detail message.
	 *
	 * @param message
	 *            the detail message
	 * @param cause
	 *            the root cause of the exception
	 */
	public CustomParseException(final String message, final Throwable cause) {
		super(message);
		this.initCause(cause);
	}

}
