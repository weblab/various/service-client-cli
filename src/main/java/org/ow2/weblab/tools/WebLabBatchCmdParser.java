/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is in charge of defining the options to be configured by the user and to parse the command line used by the user for the WebLab Batch Client.
 *
 * @author ymombrun
 * @date 2012-02-22
 */
public final class WebLabBatchCmdParser {


	/**
	 * The default number of millisecond to wait between two calls in the same thread.
	 */
	private static final String DEFAULT_TEMPO = "500";


	/**
	 * The default timeout in millisecond to let a single document being processed.
	 */
	private static final String DEFAULT_TIMEOUT = "120000";


	private static final String SYNTAX = "startup";


	private static final String DEFAULT_DEACTIVATE = Boolean.FALSE.toString();


	// Option of each parameter

	private final Option inputFolderOpt;


	private final Option interfaceOpt;


	private final Option nbThreadsOpt;


	private final Option tempoOpt;


	private final Option maxNbCallOpt;


	private final Option timeoutOpt;


	private final Option urlOpt;


	private final Option usageContextOpt;


	private final Option helpOpt;


	private final Option outputFolderOpt;


	private final Option deactivateValidationOpt;


	/**
	 * The command line that has been parsed
	 */
	private CommandLine line;


	private final Log log;


	/**
	 * The list of options
	 */
	private final Options options;



	/**
	 * Uses CLI to parse the command line.
	 * Feed each option field of the class.
	 *
	 * @param args
	 *            The command line input
	 * @throws ParseException
	 *             If an error occurs when parsing the command line
	 */
	public WebLabBatchCmdParser(final String[] args) throws ParseException {
		this.log = LogFactory.getLog(this.getClass());
		this.options = new Options();

		this.urlOpt = new Option("u", "url", true, "The URL of the service to call. This option is required.");
		this.options.addOption(this.urlOpt);

		this.interfaceOpt = new Option("i", "interface", true, "The WebLab interface name (analyser or indexer). This option is required.");
		this.options.addOption(this.interfaceOpt);

		this.inputFolderOpt = new Option("f", "inputfolder", true, "The folder in which to read XML WebLab file. Might be required for some interfaces.");
		this.inputFolderOpt.setRequired(false); // Required for analyser and indexer. Might be useless for QueueManager for instance.
		this.options.addOption(this.inputFolderOpt);

		this.outputFolderOpt = new Option("o", "outputfolder", true, "The folder in which output might be serialised. Might be required for some interfaces.");
		this.outputFolderOpt.setRequired(false);
		this.options.addOption(this.outputFolderOpt);

		this.tempoOpt = new Option("t", "tempo", true, "Temporisation in millisecond to wait between two calls in the same thread. Default value is 500 ms.");
		this.tempoOpt.setRequired(false);
		this.options.addOption(this.tempoOpt);

		this.timeoutOpt = new Option("w", "timeout", true, "Maximum time in millisecond to wait to let each document being processed. Default value is 120 s.");
		this.timeoutOpt.setRequired(false);
		this.options.addOption(this.timeoutOpt);

		this.nbThreadsOpt = new Option("p", "parallelCalls", true, "Number of parallel caller that should send request to the query. Default value is 1.");
		this.nbThreadsOpt.setRequired(false);
		this.options.addOption(this.nbThreadsOpt);

		this.usageContextOpt = new Option("c", "usageContext", true, "UsageContext to be used when calling services. Default value is the empty String.");
		this.usageContextOpt.setRequired(false);
		this.options.addOption(this.usageContextOpt);

		this.deactivateValidationOpt = new Option("v", "deactivateValidation", true,
				"Boolean that states whether or not to validate input resources. Default value is the empty false (i.e. it validates resources).");
		this.deactivateValidationOpt.setRequired(false);
		this.options.addOption(this.deactivateValidationOpt);

		this.maxNbCallOpt = new Option("m", "maxNbCall", true, "Maximum number of calls to be sent through the client (Only needed for QueueManager).");
		this.maxNbCallOpt.setRequired(false);
		this.options.addOption(this.maxNbCallOpt);

		this.helpOpt = new Option("h", "help", false, "Print this message");
		this.helpOpt.setRequired(false);
		this.options.addOption(this.helpOpt);

		try {
			this.line = new GnuParser().parse(this.options, args.clone());
		} catch (final ParseException pe) {
			new HelpFormatter().printHelp(WebLabBatchCmdParser.SYNTAX, this.options);
			throw pe;
		}
	}


	/**
	 * Check if user ask for help
	 *
	 * @return If use ask for help or not
	 */
	public boolean isHelp() {
		return this.line.hasOption(this.helpOpt.getOpt());
	}


	/**
	 * Automatically create a help message
	 *
	 * @return The help message
	 */
	public String getHelpMessage() {
		final StringWriter stringWriter = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(stringWriter);
		printWriter.println();
		printWriter.println();
		new HelpFormatter().printHelp(printWriter, HelpFormatter.DEFAULT_WIDTH, WebLabBatchCmdParser.SYNTAX, null, this.options,
				HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, null, false);
		printWriter.println();
		return stringWriter.toString();
	}


	/**
	 * Read the output folder parameter from the command line and create it if needed
	 *
	 * @return An existing, writable directory to be used to write XML resources or null
	 * @throws ParseException
	 */
	public File getOutputFolder() throws ParseException {
		final String folderValue = this.line.getOptionValue(this.outputFolderOpt.getOpt());
		if (folderValue == null) {
			return null;
		}
		final File folder = new File(folderValue);
		if (folder.exists() && !folder.isDirectory()) {
			final String message = "File " + folderValue + " exists and is not a directory.";
			this.log.error(message);
			throw new ParseException(message);
		} else if (!folder.exists() && !folder.mkdirs()) {
			final String message = "Folder " + folderValue + " cannot be created.";
			this.log.error(message);
			throw new ParseException(message);
		}

		if (!folder.canWrite()) {
			final String message = "Folder " + folderValue + " is not writeable.";
			this.log.error(message);
			throw new ParseException(message);
		}

		return folder;
	}


	/**
	 * Read the folder parameter from the command line and test its existence
	 *
	 * @return An existing, readable file or directory to be used to search XML resources
	 * @throws ParseException
	 */
	public File getInputFile() throws ParseException {
		// When called, it is required for the kind of service called
		final String folderValue = this.getRequiredOptValue(this.inputFolderOpt);
		final File folder = new File(folderValue);
		if (!folder.exists()) {
			final String message = "File " + folderValue + " does not exists.";
			this.log.error(message);
			throw new ParseException(message);
		}
		if (!folder.canRead()) {
			final String message = "File " + folderValue + " is not readable.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return folder;
	}


	/**
	 * Read the interface parameter from the command line and test if it is allowed
	 *
	 * @return The parameter in lower case
	 * @throws ParseException
	 */
	public String getInterfaceName() throws ParseException {
		return this.getRequiredOptValue(this.interfaceOpt).toLowerCase(Locale.ENGLISH);
	}


	/**
	 * Read the parallel call parameter from the command line or use default one (1). Then check that it it is valid.
	 *
	 * @return A positive integer
	 * @throws ParseException
	 */
	public int getNbThreadOption() throws ParseException {
		final String nbThreadsValue = this.line.getOptionValue(this.nbThreadsOpt.getOpt(), "1");
		final int nbThreads;
		try {
			nbThreads = Integer.parseInt(nbThreadsValue);
		} catch (final NumberFormatException nfe) {
			final String message = "Unable to get number of threads from value " + nbThreadsValue + ".";
			this.log.error(message);
			throw new CustomParseException(message, nfe);
		}
		if (nbThreads <= 0) {
			final String message = "Number of thread cannot be negative or null.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return nbThreads;
	}


	/**
	 * Read the maximum number of call parameter from the command line or use default one (100). Then check that it is valid.
	 *
	 * @return A positive integer
	 * @throws ParseException
	 */
	public int getMaxNbCallOption() throws ParseException {
		final String maxNbCallValue = this.line.getOptionValue(this.maxNbCallOpt.getOpt(), "100");
		final int nbThreads;
		try {
			nbThreads = Integer.parseInt(maxNbCallValue);
		} catch (final NumberFormatException nfe) {
			final String message = "Unable to maximum number of call from value " + maxNbCallValue + ".";
			this.log.error(message);
			throw new CustomParseException(message, nfe);
		}
		if (nbThreads <= 0) {
			final String message = "Maximum number of call cannot be negative or null.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return nbThreads;
	}


	/**
	 * Read the temporisation parameter from the command line or use default one (500). Then check that it it is valid
	 *
	 * @return A positive or null long
	 * @throws ParseException
	 */
	public long getTemporisation() throws ParseException {
		final String tempoValue = this.line.getOptionValue(this.tempoOpt.getOpt(), WebLabBatchCmdParser.DEFAULT_TEMPO);
		final long tempo;
		try {
			tempo = Long.parseLong(tempoValue);
		} catch (final NumberFormatException nfe) {
			final String message = "Unable to get temporisation from value " + tempoValue + ".";
			this.log.error(message);
			throw new CustomParseException(message, nfe);
		}
		if (tempo < 0) {
			final String message = "Temporisation " + tempo + " cannot be negative.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return tempo;
	}


	/**
	 * Read the timeout parameter from the command line or use default one (120000). Then check that it is valid.
	 *
	 * @return A positive or null long
	 * @throws ParseException
	 */
	public long getTimeoutOption() throws ParseException {
		final String timeoutValue = this.line.getOptionValue(this.timeoutOpt.getOpt(), WebLabBatchCmdParser.DEFAULT_TIMEOUT);
		final long nbSeconds;
		try {
			nbSeconds = Long.parseLong(timeoutValue);
		} catch (final NumberFormatException nfe) {
			final String message = "Unable to get number of millisecond from value " + timeoutValue + ".";
			this.log.error(message);
			throw new CustomParseException(message, nfe);
		}
		if (nbSeconds <= 0) {
			final String message = "Number of second to wait cannot be negative or null.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return nbSeconds;
	}


	/**
	 * Read the deactivateValidation parameter from the command line or use default one (false).
	 *
	 * @return A boolean
	 */
	public boolean getDeactivateValidation() {
		final String deactivateValue = this.line.getOptionValue(this.deactivateValidationOpt.getOpt(), WebLabBatchCmdParser.DEFAULT_DEACTIVATE);
		return Boolean.parseBoolean(deactivateValue);
	}


	/**
	 * Read the url parameter and that it is a valid URL
	 *
	 * @return The URL parameter
	 * @throws ParseException
	 */
	public URL getUrl() throws ParseException {
		final String urlValue = this.getRequiredOptValue(this.urlOpt);
		final URL url;
		try {
			url = new URL(urlValue);
		} catch (final MalformedURLException murle) {
			final String message = "Url " + urlValue + " is not valid.";
			this.log.error(message);
			throw new CustomParseException(message, murle);
		}
		return url;
	}


	/**
	 * Read the usageContext parameter from the command line or use default one (the empty string)
	 *
	 * @return The usageContext
	 */
	public String getUsageContextOption() {
		return this.line.getOptionValue(this.usageContextOpt.getOpt(), "");
	}


	/**
	 * Get the value of an option that has been fed though command line. If null, throws a ParseException
	 *
	 * @param opt
	 *            The option to get value
	 * @return A non null value
	 * @throws ParseException
	 */
	private String getRequiredOptValue(final Option opt) throws ParseException {
		final String optValue = this.line.getOptionValue(opt.getOpt());
		if (optValue == null) {
			final String message = "Unable to get the parameter " + opt.getOpt() + " for your command line.";
			this.log.error(message);
			throw new ParseException(message);
		}
		return optValue;
	}

}
