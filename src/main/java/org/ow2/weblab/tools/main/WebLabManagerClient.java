/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.main;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.tools.manager.utils.cmd.AManagerClient;
import org.ow2.weblab.tools.manager.utils.cmd.DeleteInstanceCmd;
import org.ow2.weblab.tools.manager.utils.cmd.ListChainsCmd;
import org.ow2.weblab.tools.manager.utils.cmd.ListInstancesCmd;
import org.ow2.weblab.tools.manager.utils.cmd.PauseCmd;
import org.ow2.weblab.tools.manager.utils.cmd.StartCmd;
import org.ow2.weblab.tools.manager.utils.cmd.StatusCmd;
import org.ow2.weblab.tools.manager.utils.cmd.StopCmd;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;


/**
 * This one of the main classes of the project.
 * It uses JCommander to parse args from the command line, calls the required chain manager method and show the results in the standard output.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public final class WebLabManagerClient {


	private final Log log;


	/**
	 * The command recognised in the command line (i.e. the main action, which is the first word of the commandline)
	 */
	private final AManagerClient theCommand;


	public WebLabManagerClient(final String[] args) throws MissingCommandException {
		this.log = LogFactory.getLog(this.getClass());
		this.log.debug("Received args: " + Arrays.deepToString(args));

		final JCommander parser = new JCommander();

		parser.setProgramName("weblab-manager-client");

		// Add every command to a map and then to the commander
		final Map<String, AManagerClient> commands = new HashMap<>();
		commands.put(ListChainsCmd.LIST_CHAINS, new ListChainsCmd());
		commands.put(ListInstancesCmd.LIST_INSTANCES, new ListInstancesCmd());
		commands.put(StartCmd.START, new StartCmd());
		commands.put(StatusCmd.STATUS, new StatusCmd());
		commands.put(PauseCmd.PAUSE, new PauseCmd());
		commands.put(StopCmd.STOP, new StopCmd());
		commands.put(DeleteInstanceCmd.DELETE_INSTANCE, new DeleteInstanceCmd());

		for (final AManagerClient command : commands.values()) {
			parser.addCommand(command);
		}

		if ((args == null) || (args.length == 0)) {
			parser.usage();
			final String message = "Not a single command line argument.";
			throw new NullPointerException(message);
		}

		try {
			parser.parse(args);
		} catch (final RuntimeException re) {
			parser.usage();
			throw re;
		}

		final String parsedCommand = parser.getParsedCommand();
		this.log.debug("Recognised command '" + parsedCommand + "'.");
		this.theCommand = commands.get(parsedCommand);
	}


	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		try {
			final WebLabManagerClient client = new WebLabManagerClient(args);
			final String message = client.run();
			LogFactory.getLog(WebLabManagerClient.class).debug(message);
			System.out.println(message);
		} catch (final RuntimeException e) {
			LogFactory.getLog(WebLabManagerClient.class).debug(e.getMessage(), e);
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		}

		System.exit(0);
	}



	public String run() {
		return this.theCommand.callServiceAndGetResults();
	}

}
