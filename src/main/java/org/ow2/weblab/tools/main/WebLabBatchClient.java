/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.main;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.cli.ParseException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.Configurable;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.QueueManager;
import org.ow2.weblab.tools.WebLabBatchCmdParser;
import org.ow2.weblab.tools.batchcaller.AbstractBatchCaller;
import org.ow2.weblab.tools.batchcaller.AbstractBatchCaller.Result;
import org.ow2.weblab.tools.batchcaller.AnalyserBatchCaller;
import org.ow2.weblab.tools.batchcaller.ConfigurableBatchCaller;
import org.ow2.weblab.tools.batchcaller.IndexerBatchCaller;
import org.ow2.weblab.tools.batchcaller.QueueManagerBatchCaller;

/**
 * This one of the main classes of the project.
 * It launches an instance of BatchCommandLineParser and then executes the required AbstractBatchCaller.
 *
 * This class is meant for calling many times the same service instance in order to (1) get its results (for instance analysing a folder of resources) and/or
 * (2) test the service hardness by calling it in parallel by many threads.
 *
 * @author ymombrun
 * @date 2012-02-15
 */
public final class WebLabBatchClient {



	/**
	 * Hidden, empty and useless constructor
	 */
	private WebLabBatchClient() {
		// Should not be called
	}


	private static final Log LOG = LogFactory.getLog(WebLabBatchClient.class);


	private static final Set<String> needInput = new HashSet<>(Arrays.asList(Analyser.class.getSimpleName().toLowerCase(), Configurable.class
			.getSimpleName().toLowerCase(), Indexer.class.getSimpleName().toLowerCase()));


	private static final Set<String> mayNeedOutput = new HashSet<>(Arrays.asList(Analyser.class.getSimpleName().toLowerCase(), QueueManager.class
			.getSimpleName().toLowerCase()));


	/**
	 * Parses the command line arguments and calls the requested services.
	 *
	 * @param args
	 *            The command line as described in {@link WebLabBatchCmdParser}
	 */
	public static void main(final String[] args) {

		WebLabBatchClient.LOG.debug("WebLabBatchClient called with the following parameters: " + Arrays.deepToString(args));

		// Create a dedicated command line parser that will handle the various options.
		final WebLabBatchCmdParser parser;
		try {
			parser = new WebLabBatchCmdParser(args);
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.debug("Unable to parse the request: " + pe.getMessage(), pe);
			System.exit(1);
			// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
			return;
		}


		if (parser.isHelp()) {
			WebLabBatchClient.LOG.info(parser.getHelpMessage());
			System.exit(0);
			// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
			return;
		}


		// Get common options needed to instantiate any kind of service callers. And fail if any is missing
		final URL url;
		final String interfaceName;
		final int nbThreads;
		final long tempo, timeout;
		final String usageContext;
		final boolean deactivateValidation;
		try {
			url = WebLabBatchClient.readUrlParam(parser);
			interfaceName = WebLabBatchClient.readInterfaceNameParam(parser);
			tempo = WebLabBatchClient.readTeporisationParam(parser);
			nbThreads = WebLabBatchClient.readNbThreadParam(parser);
			timeout = WebLabBatchClient.readTimeoutParam(parser);
			usageContext = parser.getUsageContextOption();
			deactivateValidation = parser.getDeactivateValidation();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.debug(pe.getMessage(), pe);
			WebLabBatchClient.LOG.error(parser.getHelpMessage());
			System.exit(1);
			// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
			return;
		}


		// Get input file or folder, only if needed
		final File inputFileOrFolder;
		if (WebLabBatchClient.needInput.contains(interfaceName)) {
			try {
				inputFileOrFolder = parser.getInputFile();
				if (!interfaceName.equals(Configurable.class.getName().toLowerCase()) && !inputFileOrFolder.isDirectory()) {
					final String message = "File " + inputFileOrFolder + " is not a directory.";
					WebLabBatchClient.LOG.error(message);
					throw new ParseException(message);
				}
			} catch (final ParseException pe) {
				WebLabBatchClient.LOG.error("Fails to read value of input folder parameter. Which is required for this interface (" + interfaceName + ").");
				WebLabBatchClient.LOG.debug(pe.getMessage(), pe);
				WebLabBatchClient.LOG.error(parser.getHelpMessage());
				System.exit(1);
				// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
				return;
			}
		} else {
			inputFileOrFolder = null;
		}

		// Get output file or folder, only if needed
		final File outputFileOrFolder;
		if (WebLabBatchClient.mayNeedOutput.contains(interfaceName)) {
			try {
				outputFileOrFolder = parser.getOutputFolder();
			} catch (final ParseException pe) {
				WebLabBatchClient.LOG.error("Fails to read value of output folder parameter. Which is maybe be needed for this interface (" + interfaceName
						+ ").");
				WebLabBatchClient.LOG.debug(pe.getMessage(), pe);
				WebLabBatchClient.LOG.error(parser.getHelpMessage());
				System.exit(1);
				// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
				return;
			}
		} else {
			outputFileOrFolder = null;
		}

		final int maxNbCall;
		if (interfaceName.equalsIgnoreCase(QueueManager.class.getSimpleName())) {
			try {
				maxNbCall = parser.getMaxNbCallOption();
			} catch (final ParseException pe) {
				WebLabBatchClient.LOG.error("Fails to read value of maximum number of call parameter. Which is needed for this interface (" + interfaceName
						+ ").");
				WebLabBatchClient.LOG.debug(pe.getMessage(), pe);
				WebLabBatchClient.LOG.error(parser.getHelpMessage());
				System.exit(1);
				// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
				return;
			}
		} else {
			maxNbCall = -1; // Not used
		}


		// Create the tester instance given interface selected
		WebLabBatchClient.LOG
				.info("Initialises tests. This may take a while (espacially if input folder contains a lot of files and if validation is activated).");
		final AbstractBatchCaller tester;
		if (interfaceName.equalsIgnoreCase(Analyser.class.getSimpleName())) {
			tester = new AnalyserBatchCaller(nbThreads, tempo, url, inputFileOrFolder, deactivateValidation, usageContext, outputFileOrFolder, timeout);
		} else if (interfaceName.equalsIgnoreCase(Configurable.class.getSimpleName())) {
			tester = new ConfigurableBatchCaller(nbThreads, tempo, url, inputFileOrFolder, deactivateValidation, usageContext, timeout);
		} else if (interfaceName.equalsIgnoreCase(Indexer.class.getSimpleName())) {
			tester = new IndexerBatchCaller(nbThreads, tempo, url, inputFileOrFolder, deactivateValidation, usageContext, timeout);
		} else if (interfaceName.equalsIgnoreCase(QueueManager.class.getSimpleName())) {
			tester = new QueueManagerBatchCaller(nbThreads, tempo, url, deactivateValidation, usageContext, outputFileOrFolder, timeout, maxNbCall);
		} else {
			WebLabBatchClient.LOG.error("Unhandled interface " + interfaceName + ".");
			System.exit(1);
			// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
			return;
		}

		WebLabBatchClient.LOG.info("Start web services calls.");
		try {
			tester.executeTest();
		} catch (final InterruptedException e) {
			WebLabBatchClient.LOG.error("Global timeout of " + timeout + " ms has been reached before processing every request.", e);
			System.exit(1);
			// Add a return statement to prevent from a compile error since it does not see that the code after will not be executed...
			return;
		}

		WebLabBatchClient.LOG.info("Start validating results.");
		final Result result = tester.validateTest();
		switch (result) {
			case VALID:
				WebLabBatchClient.LOG.info("Service " + url + " has been successfully validated.");
				System.exit(0);
				break;
			case UNFINISHED_EXECUTION:
				WebLabBatchClient.LOG.error("Service " + url + " has not been validated since at least one of the process has not been completed in time.");
				System.exit(1);
				break;
			case EXECUTION_ERROR:
				WebLabBatchClient.LOG.error("Service " + url + " has not been validated since at least one of the process has thrown an exception.");
				System.exit(1);
				break;
			case INVALID: //$FALL-THROUGH$
			default:
				WebLabBatchClient.LOG.error("Service " + url + " has not been validated since at least one of the response is not valid.");
				System.exit(1);
				break;
		}

		WebLabBatchClient.LOG.error("Please report a bug, you shall never see this message!!!");
		System.exit(1);
	}


	private static long readTimeoutParam(final WebLabBatchCmdParser parser) throws ParseException {
		try {
			return parser.getTimeoutOption();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.error("Fails to read value of timeout parameter.");
			throw pe;
		}
	}


	private static int readNbThreadParam(final WebLabBatchCmdParser parser) throws ParseException {
		try {
			return parser.getNbThreadOption();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.error("Fails to read value of parallelisation parameter.");
			throw pe;
		}
	}


	private static long readTeporisationParam(final WebLabBatchCmdParser parser) throws ParseException {
		try {
			return parser.getTemporisation();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.error("Fails to read value of temporisation parameter.");
			throw pe;
		}
	}


	private static String readInterfaceNameParam(final WebLabBatchCmdParser parser) throws ParseException {
		try {
			return parser.getInterfaceName();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.error("Fails to read interface name which is a compulsory parameter.");
			throw pe;
		}
	}


	private static URL readUrlParam(final WebLabBatchCmdParser parser) throws ParseException {
		try {
			return parser.getUrl();
		} catch (final ParseException pe) {
			WebLabBatchClient.LOG.error("Fails to read URL which is a compulsory parameter.");
			throw pe;
		}
	}


}
