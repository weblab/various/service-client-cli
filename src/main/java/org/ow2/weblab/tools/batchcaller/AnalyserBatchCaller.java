/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.batchcaller;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ServiceUtil;
import org.ow2.weblab.core.helpers.validator.InMemoryExecutor;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.Analyser_Service;
import org.ow2.weblab.tools.callable.AnalyserCallable;


/**
 * This specific implementation of AbstractBatchCaller works for the analyser interface.
 *
 * It loop over inputFolder, sending each resource of to the service and checking that results are valid.
 *
 * @author ymombrun
 * @date 2012-02-22
 */
public final class AnalyserBatchCaller extends AbstractBatchCaller {


	/**
	 * The usageContext to each in each call
	 */
	private final String usageContext;


	/**
	 * The analyser to call
	 */
	private final Analyser analyser;


	/**
	 * The collection that contain input files to be processed
	 */
	private final List<File> inputFiles;


	/**
	 * The list of results coming from analyser callables
	 */
	private List<Future<Resource>> futures;


	/**
	 * The validator to be used for in and out put
	 */
	private final Validator validator;


	/**
	 * Whether or not to generate output files
	 */
	private final boolean generateOutput;


	/**
	 * The folder to be used as root for outputs
	 */
	private final File outputFolder;


	/**
	 * Root folder where files are searched.
	 */
	private final String inputFolderRoot;


	private final List<AnalyserCallable> analysers;


	/**
	 * Total time to wait for the whole processing;
	 */
	private final long totalTimeout;


	/**
	 * @param nbThreads
	 *            The number of thread in the thread factory, i.e. the number of parallel calls.
	 * @param temporisation
	 *            The amount of milliseconds to wait before each call in a thread queue.
	 * @param endpointAddress
	 *            The address of the service to be called.
	 * @param inputFolder
	 *            Folder to search of resources.
	 * @param deactivateValidation
	 *            Whether or not to skip the validation of input resources.
	 * @param usageContext
	 *            The usageContext to be used in each call.
	 * @param outputFolder
	 *            The folder to be used to store output (might be null if they are not needed).
	 * @param timeout
	 *            Time to wait The amount of milliseconds to wait for a single processing.
	 */
	public AnalyserBatchCaller(final int nbThreads, final long temporisation, final URL endpointAddress, final File inputFolder,
			final boolean deactivateValidation, final String usageContext, final File outputFolder, final long timeout) {
		super(nbThreads, temporisation, endpointAddress, timeout);
		this.usageContext = usageContext;
		this.validator = new InMemoryExecutor();
		this.generateOutput = (outputFolder != null);
		this.outputFolder = outputFolder;

		final IOFileFilter filter;
		if (deactivateValidation) {
			filter = FileFilterUtils.suffixFileFilter("xml");
		} else {
			filter = FileFilterUtils.and(FileFilterUtils.suffixFileFilter("xml"), new ValidResourceFilter(this.validator));
		}

		this.inputFiles = new LinkedList<>(FileUtils.listFiles(inputFolder, filter, TrueFileFilter.TRUE));
		this.inputFolderRoot = inputFolder.getAbsolutePath();

		// Total timeout is the number of files multiplied by the time for each call (single call timeout + tempo), divided by the number of parallel threads
		// and multiplied by the factor
		this.totalTimeout = (long) (((this.timeout.longValue() + this.tempo) * this.inputFiles.size() * AbstractBatchCaller.TOTAL_TIMEOUT_INCREASE) / this.nbThreads);

		this.analyser = this.initWS();
		this.analysers = Collections.synchronizedList(new LinkedList<AnalyserCallable>());
		this.log.info("AnalyserBatchCaller initialised with " + this.inputFiles.size() + " resources to be processed by " + nbThreads + " threads in parallel.");
	}


	/**
	 * Initialises an analyser stub
	 *
	 * @return An analyser client
	 */
	private Analyser initWS() {
		final Analyser_Service confService = new Analyser_Service(Analyser_Service.class.getClassLoader().getResource("services/WebLab.wsdl"),
				Analyser_Service.SERVICE);
		final Analyser theAnalyser = confService.getAnalyserPort();
		ServiceUtil.setEndpointAddress(theAnalyser, this.endpointAddress.toString(), "process");
		this.setServiceTimeout(theAnalyser);
		return theAnalyser;
	}


	@Override
	public void executeTest() throws InterruptedException {
		this.log.debug("Start creating analysers.");
		final WebLabMarshaller wlm = new WebLabMarshaller();
		for (final File f : this.inputFiles) {
			Resource res;
			try {
				res = wlm.unmarshal(f, Resource.class);
			} catch (final WebLabCheckedException wlce) {
				this.log.warn("Unable to unmarshall a file that has been validated before (" + f.getAbsolutePath() + ").", wlce);
				throw new WebLabUncheckedException("Unable to unmarshall a file that has been validated before (" + f.getAbsolutePath() + ").", wlce);
			}
			this.analysers.add(new AnalyserCallable(this.tempo, res, this.usageContext, this.analyser));
		}

		this.log.debug("Analysers created. Invoking them all. This should last at max " + this.totalTimeout + " milliseconds.");

		this.futures = this.executor.invokeAll(this.analysers, this.totalTimeout, TimeUnit.MILLISECONDS);

		this.log.debug("Analysers invoked.");
	}


	@Override
	public Result validateTest() {
		this.log.debug("Start validating results.");

		if ((this.futures == null) || this.futures.isEmpty()) {
			this.log.error("No Future!!");
			return Result.UNFINISHED_EXECUTION;
		}

		Result harderResult = Result.VALID;

		int nbCall = -1;
		for (final Future<Resource> future : this.futures) {
			nbCall++;
			if (!future.isDone()) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			final Resource res;
			try {
				res = future.get();
			} catch (final InterruptedException ie) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ie);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			} catch (final ExecutionException ee) {
				final AnalyserCallable callable = this.analysers.get(nbCall);
				this.log.error("An error occured for call number " + nbCall + ". The input resource was " + callable.getResource().getUri() + " ("
						+ this.inputFiles.get(nbCall).getPath() + "). Error was " + callable.getException().getMessage());
				this.log.debug("ExecutionException was: ", ee);
				this.log.debug("Inner exception was: ", callable.getException());
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.INVALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.EXECUTION_ERROR;
				}
				continue;
			} catch (final CancellationException ce) {
				this.log.error("Timeout has been reached before the call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ce);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			this.log.debug("Try validating output number " + nbCall + " (" + res.getUri() + ") .");

			final boolean validate = this.validator.validate(res);
			if (!validate) {
				this.log.error("The result number " + nbCall + " was not valid (" + res.getUri() + " at " + this.inputFiles.get(nbCall).getPath() + ").");
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.INVALID;
				}
				continue;
			}

			if (this.generateOutput) {
				this.marshallOutput(res, this.inputFiles.get(nbCall));
			}
		}

		this.log.debug("End of validateTest.");

		return harderResult;
	}


	/**
	 * Write the resource in a file that have the same name and the same hierarchy than the inputfiles.
	 *
	 * @param res
	 *            The resource to marshal
	 * @param inputFile
	 *            The input file used as base name for the output file
	 */
	private void marshallOutput(final Resource res, final File inputFile) {
		final File outputFile = new File(this.outputFolder, inputFile.getAbsolutePath().replace(this.inputFolderRoot, ""));
		final File parent = outputFile.getParentFile();
		if (!parent.exists() && !parent.mkdirs()) {
			this.log.error("Unable to marshall resource (" + res.getUri() + ") + in file " + outputFile.getAbsolutePath() + ". Parent folder can't be created.");
		}
		try {
			new WebLabMarshaller().marshalResource(res, outputFile);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn("Unable to marshall resource (" + res.getUri() + ") + in file " + outputFile.getAbsolutePath() + ".");
		}
	}

}
