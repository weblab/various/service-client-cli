/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.batchcaller;

import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.BindingProvider;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;


/**
 * This abstract class should be inherited for specific web services.
 *
 * It contains new method, one in charge of calling the service through the executor inside and another one in charge of validating results of the execution
 * (for instance through the use of resource-validator).
 *
 * @author ymombrun
 * @date 2012-02-22
 */
public abstract class AbstractBatchCaller {


	private static final String CONNECT_TIMEOUT_PROP = "com.sun.xml.ws.connect.timeout";


	/**
	 * Factor to apply to the basic total timeout (nb of call x (single call timeout + tempo) / nb thread) to take into account Java/Executors time spent
	 */
	protected static final double TOTAL_TIMEOUT_INCREASE = 1.2;


	public enum Result {
		VALID, UNFINISHED_EXECUTION, EXECUTION_ERROR, INVALID
	}



	/**
	 * The logger used inside
	 */
	protected final Log log;


	/**
	 * The executor used to dispatch web services calls. It is a fixed thread pool factory.
	 */
	protected final ExecutorService executor;


	/**
	 * The temporisation to wait before each call (for each thread)
	 */
	protected final long tempo;


	/**
	 * The maximum time of a single processing;
	 */
	protected final Long timeout;


	/**
	 * The address of the endpoint to be called
	 */
	protected final URL endpointAddress;


	/**
	 * The number of thread in the thread factory, i.e. the number of parallel calls.
	 */
	protected final int nbThreads;


	/**
	 * @param nbThreads
	 *            The number of thread in the thread factory, i.e. the number of parallel calls.
	 * @param temporisation
	 *            The amount of milliseconds to wait before each call in a thread queue.
	 * @param endpointAddress
	 *            The address of the service to be called.
	 * @param timeout
	 *            Time to wait The amount of milliseconds to wait for a single processing.
	 */
	public AbstractBatchCaller(final int nbThreads, final long temporisation, final URL endpointAddress, final long timeout) {
		this.log = LogFactory.getLog(this.getClass());
		this.tempo = temporisation;
		this.timeout = Long.valueOf(timeout);
		this.endpointAddress = endpointAddress;
		this.nbThreads = nbThreads;
		this.executor = Executors.newFixedThreadPool(nbThreads);
	}


	/**
	 * Call the service as many time as requested and wait before failing if not finished.
	 *
	 * @throws InterruptedException
	 */
	public abstract void executeTest() throws InterruptedException;


	/**
	 * Checks that every answer web services calls are valid
	 *
	 * @return a Result whether test are valid, invalid or if an error occurs
	 */
	public abstract Result validateTest();



	/**
	 * @param port
	 *            An instance of <code>BindingProvider</code> to have it's timeout to be changed.
	 */
	public void setServiceTimeout(final Object port) {
		if (!(port instanceof BindingProvider)) {
			throw new WebLabUncheckedException("Object: " + port + " doesn't appear to be a valid port.");
		}
		final Map<String, Object> context = ((BindingProvider) port).getRequestContext();
		context.put(AbstractBatchCaller.CONNECT_TIMEOUT_PROP, this.timeout);
	}


}
