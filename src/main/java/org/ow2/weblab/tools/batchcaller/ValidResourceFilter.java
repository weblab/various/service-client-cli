/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.batchcaller;

import java.io.File;

import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.model.Resource;

import com.hp.hpl.jena.shared.ClosedException;

/**
 * A file filter that checks that the resource is valid.
 *
 * @author ymombrun
 * @date 2012-02-23
 */
public final class ValidResourceFilter implements IOFileFilter {


	/**
	 * The logger used inside
	 */
	private final Log log;


	/**
	 * The validator to be used on unmarshalled resources
	 */
	private final Validator validator;


	/**
	 * The marshaller used to unmarshal files
	 */
	private final WebLabMarshaller marshaller;


	/**
	 * @param validator
	 *            The resource validator to be used to validate resources
	 */
	public ValidResourceFilter(final Validator validator) {
		this.validator = validator;
		this.marshaller = new WebLabMarshaller(true);
		this.log = LogFactory.getLog(this.getClass());
	}


	@Override
	public boolean accept(final File file) {

		this.log.trace("Unmarshalling resource file " + file.getAbsolutePath() + ".");

		final Resource res;
		try {
			res = this.marshaller.unmarshal(file, Resource.class);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn("Resource cannot be loaded from file " + file.getAbsolutePath() + ". It will not be used.");
			return false;
		}

		this.log.trace("File " + file.getAbsolutePath() + " contains an XSD valid resource " + res.getUri() + ". Use validators for better validation.");

		boolean valid;
		try {
			valid = this.validator.validate(res);
		} catch (final ClosedException ce) {
			this.log.debug("Catching a ClosedException which is a Jena bug. Retry.", ce);
			// Workaround for WEBLAB-754 (let 3 tries before considering a resource as invalid due to a ClosedException
			try {
				valid = this.validator.validate(res);
			} catch (final ClosedException ce2) {
				this.log.debug("Catching twice a ClosedException which is a Jena bug. Retry.", ce2);
				try {
					valid = this.validator.validate(res);
				} catch (final ClosedException ce3) {
					this.log.warn("Catching again a ClosedException which is a Jena bug. Considering the resource as invalid.", ce3);
					valid = false;
				}
			}
		}

		if (!valid) {
			this.log.warn("Resource " + res.getUri() + " loaded from file " + file.getAbsolutePath() + " is not valid. It will not be used.");
		}

		return valid;
	}


	@Override
	public boolean accept(final File dir, final String name) {
		return this.accept(new File(dir, name));
	}

}
