/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.batchcaller;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ServiceUtil;
import org.ow2.weblab.core.helpers.validator.InMemoryExecutor;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.Indexer_Service;
import org.ow2.weblab.core.services.indexer.IndexReturn;
import org.ow2.weblab.tools.callable.IndexerCallable;

/**
 *
 * This specific implementation of AbstractBatchCaller works for the indexer interface.
 *
 * It loop over inputFolder, sending each resource of to the service.
 *
 * @author ymombrun
 * @date 2012-02-22
 */
public final class IndexerBatchCaller extends AbstractBatchCaller {


	/**
	 * The usageContext to each in each call
	 */
	private final String usageContext;


	/**
	 * The collection that contain input files to be processed
	 */
	private final List<File> inputFiles;


	/**
	 * The list of results coming from indexer callables
	 */
	private List<Future<IndexReturn>> futures;


	/**
	 * The indexer to be called
	 */
	private final Indexer indexer;


	/**
	 * The list of IndexerCallable to be called.
	 */
	private final List<IndexerCallable> indexers;


	/**
	 * Total time to wait for the whole processing;
	 */
	private final long totalTimeout;



	/**
	 * @param nbThreads
	 *            The number of thread in the thread factory, i.e. the number of parallel calls
	 * @param temporisation
	 *            The amount of millisecond to wait before each call in a thread queue
	 * @param endpointAddress
	 *            The address of the service to be called
	 * @param inputFolder
	 *            Folder to search of resources
	 * @param deactivateValidation
	 *            Whether or not to skip validation of input resources.
	 * @param usageContext
	 *            The usageContext to be used in each call
	 * @param timeout
	 *            The amount of milliseconds to wait for a single indexing.
	 */
	public IndexerBatchCaller(final int nbThreads, final long temporisation, final URL endpointAddress, final File inputFolder,
			final boolean deactivateValidation, final String usageContext, final long timeout) {
		super(nbThreads, temporisation, endpointAddress, timeout);
		this.usageContext = usageContext;

		final IOFileFilter filter;
		if (deactivateValidation) {
			filter = FileFilterUtils.suffixFileFilter("xml");
		} else {
			filter = FileFilterUtils.and(FileFilterUtils.suffixFileFilter("xml"), new ValidResourceFilter(new InMemoryExecutor()));
		}
		this.inputFiles = new LinkedList<>(FileUtils.listFiles(inputFolder, filter, TrueFileFilter.TRUE));
		this.indexers = Collections.synchronizedList(new LinkedList<IndexerCallable>());
		this.indexer = this.initWS();

		// Total timeout is the number of files multiplied by the time for each call (single call timeout + tempo), divided by the number of parallel threads
		// and multiplied by the factor
		this.totalTimeout = (long) (((this.timeout.longValue() + this.tempo) * this.inputFiles.size() * AbstractBatchCaller.TOTAL_TIMEOUT_INCREASE) / this.nbThreads);

		this.log.info("IndexerBatchCaller initialised with " + this.inputFiles.size() + " resources to be indexed by " + nbThreads
				+ " threads in parallel. Total timeout is " + this.totalTimeout + " ms.");
	}


	/**
	 * Initialises an indexer stub
	 *
	 * @return An indexer client
	 */
	private Indexer initWS() {
		final Indexer_Service confService = new Indexer_Service(Indexer_Service.class.getClassLoader().getResource("services/WebLab.wsdl"),
				Indexer_Service.SERVICE);
		final Indexer theIndexer = confService.getIndexerPort();
		ServiceUtil.setEndpointAddress(theIndexer, this.endpointAddress.toString(), "index");
		this.setServiceTimeout(theIndexer);
		return theIndexer;
	}


	@Override
	public void executeTest() throws InterruptedException {
		final WebLabMarshaller wlm = new WebLabMarshaller();
		for (final File f : this.inputFiles) {
			final Resource res;
			try {
				res = wlm.unmarshal(f, Resource.class);
			} catch (final WebLabCheckedException wlce) {
				this.log.warn("Unable to unmarshall file: " + f.getAbsolutePath());
				continue;
			}
			this.indexers.add(new IndexerCallable(this.tempo, res, this.usageContext, this.indexer));
		}

		this.futures = this.executor.invokeAll(this.indexers, this.totalTimeout, TimeUnit.MILLISECONDS);
	}


	@Override
	public Result validateTest() {
		if ((this.futures == null) || this.futures.isEmpty()) {
			this.log.error("No Future !!");
			return Result.UNFINISHED_EXECUTION;
		}

		Result harderResult = Result.VALID;
		int nbCall = -1;
		for (final Future<IndexReturn> future : this.futures) {
			nbCall++;

			if (!future.isDone()) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			this.log.debug("Call number " + nbCall + " is finished. Validate its output.");

			final IndexReturn indexReturn;
			try {
				indexReturn = future.get();
			} catch (final InterruptedException ie) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ie);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			} catch (final ExecutionException ee) {
				final IndexerCallable callable = this.indexers.get(nbCall);
				this.log.error("An error occured for call number " + nbCall + ". The input resource was " + callable.getResource().getUri() + " ("
						+ this.inputFiles.get(nbCall).getPath() + "). Error was " + callable.getException().getMessage());
				this.log.debug("ExecutionException was: ", ee);
				this.log.debug("Inner exception was: ", callable.getException());
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.INVALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.EXECUTION_ERROR;
				}
				continue;
			} catch (final CancellationException ce) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ce);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			if (indexReturn == null) {
				this.log.error("The result number " + nbCall + " was null for file " + this.inputFiles.get(nbCall).getPath() + ").");
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.INVALID;
				}
				continue;
			}
		}

		this.log.debug("End of validateTest.");

		return harderResult;
	}

}
