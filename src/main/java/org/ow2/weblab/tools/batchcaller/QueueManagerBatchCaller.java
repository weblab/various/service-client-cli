/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.batchcaller;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ServiceUtil;
import org.ow2.weblab.core.helpers.validator.InMemoryExecutor;
import org.ow2.weblab.core.helpers.validator.Validator;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.QueueManager;
import org.ow2.weblab.core.services.QueueManager_Service;
import org.ow2.weblab.tools.callable.QueueManagerCallable;


/**
 * This specific implementation of AbstractBatchCaller works for the queue manager interface.
 *
 * It calls the queueManager service until it catches its first NoMoreResource Exception or its second any Exception.
 *
 * @author ymombrun
 * @date 2013-04-29
 */
public final class QueueManagerBatchCaller extends AbstractBatchCaller {


	/**
	 * The usageContext to each in each call
	 */
	private final String usageContext;


	/**
	 * The queueManager to call
	 */
	private final QueueManager queueManager;


	/**
	 * The list of results coming from queueManager callables
	 */
	private final List<Future<Resource>> futures;


	/**
	 * The validator to be used for output
	 */
	private final Validator validator;


	/**
	 * Whether or not to generate output files
	 */
	private final boolean generateOutput;


	/**
	 * The folder to be used as root for outputs
	 */
	private final File outputFolder;


	/**
	 * The list of callable to calls
	 */
	private final List<QueueManagerCallable> queueManagers;


	/**
	 * Total time to wait for the whole processing;
	 */
	private final long totalTimeout;


	/**
	 * The maximum number of call to send (modulo the number of threads).
	 */
	private final int maxCalls;


	/**
	 * @param nbThreads
	 *            The number of thread in the thread factory, i.e. the number of parallel calls.
	 * @param temporisation
	 *            The amount of milliseconds to wait before each call in a thread queue.
	 * @param endpointAddress
	 *            The address of the service to be called.
	 * @param deactivateValidation
	 *            Whether or not to skip the validation of input resources.
	 * @param usageContext
	 *            The usageContext to be used in each call.
	 * @param outputFolder
	 *            The folder to be used to store output (might be null if they are not needed).
	 * @param timeout
	 *            The amount of milliseconds to wait for a single processing.
	 * @param maxCalls
	 *            The maximum number of resource to be retrieved
	 */
	public QueueManagerBatchCaller(final int nbThreads, final long temporisation, final URL endpointAddress, final boolean deactivateValidation, final String usageContext, final File outputFolder,
			final long timeout, final int maxCalls) {
		super(nbThreads, temporisation, endpointAddress, timeout);
		this.usageContext = usageContext;
		this.validator = new InMemoryExecutor();
		this.generateOutput = (outputFolder != null);
		this.outputFolder = outputFolder;
		this.maxCalls = maxCalls;


		// Total timeout is the time for each call (single call timeout + tempo) and multiplied by the factor
		this.totalTimeout = (long) (((this.timeout.longValue() + this.tempo) * AbstractBatchCaller.TOTAL_TIMEOUT_INCREASE) / this.nbThreads);

		this.futures = new LinkedList<>();

		this.queueManager = this.initWS();
		this.queueManagers = Collections.synchronizedList(new LinkedList<QueueManagerCallable>());
		this.log.info("QueueManagerCallable initialised with " + nbThreads + " threads in parallel.");
	}


	/**
	 * Initialises an queueManager stub
	 *
	 * @return An queueManager client
	 */
	private QueueManager initWS() {
		final QueueManager_Service confService = new QueueManager_Service(QueueManager_Service.class.getClassLoader().getResource("services/WebLab.wsdl"), QueueManager_Service.SERVICE);
		final QueueManager theQM = confService.getQueueManagerPort();
		ServiceUtil.setEndpointAddress(theQM, this.endpointAddress.toString(), "nextResource");
		this.setServiceTimeout(theQM);
		return theQM;
	}


	@Override
	public void executeTest() throws InterruptedException {
		this.log.debug("Start creating queueManagers.");

		boolean errorOccured = false;

		// Call the various queueManager until an error occurs (timeout, no more resource or any other) or maxCall reached
		outer: for (int nbCall = 0; nbCall < this.maxCalls; nbCall += this.nbThreads) {
			final List<QueueManagerCallable> toBeCalled = new LinkedList<>();
			for (int i = 0; i < this.nbThreads; i++) {
				toBeCalled.add(new QueueManagerCallable(this.tempo, this.usageContext, this.queueManager));
			}
			this.queueManagers.addAll(toBeCalled);
			final List<Future<Resource>> results = this.executor.invokeAll(toBeCalled, this.totalTimeout, TimeUnit.MILLISECONDS);
			this.futures.addAll(results);
			for (final Future<Resource> future : results) {
				final Resource res;
				try {
					res = future.get(); // To throw an exception if the nextResource call was not returning a NextResourceReturn
				} catch (final Exception e) {
					this.log.info("One error (might be timeout) caught on queuemanagers. Stop calling them.");
					errorOccured = true;
					break outer;
				}
				if (res == null) {
					this.log.info("One response was null. Assuming that it is an error and stop calling queuemanagers.");
					errorOccured = true;
					break outer;
				}
			}
			this.log.debug("New for loop to retrieve the next " + this.nbThreads + " resources.");
		}
		if (!errorOccured) {
			this.log.info(this.maxCalls + " resources retrieved by calling the queueManagers.");
		}

	}


	@Override
	public Result validateTest() {
		this.log.debug("Start validating results.");

		if ((this.futures == null) || this.futures.isEmpty()) {
			this.log.error("No Future!!");
			return Result.UNFINISHED_EXECUTION;
		}

		Result harderResult = Result.VALID;

		int nbCall = -1;
		for (final Future<Resource> future : this.futures) {
			nbCall++;
			if (!future.isDone()) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			final Resource res;
			try {
				res = future.get();
			} catch (final InterruptedException ie) {
				this.log.error("Timeout has been reached for call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ie);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			} catch (final ExecutionException ee) {
				final QueueManagerCallable callable = this.queueManagers.get(nbCall);
				this.log.error("An error occured for call number " + nbCall + ". Error was " + callable.getException().getMessage());
				this.log.debug("ExecutionException was: ", ee);
				this.log.debug("Inner exception was: ", callable.getException());
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.INVALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.EXECUTION_ERROR;
				}
				continue;
			} catch (final CancellationException ce) {
				this.log.error("Timeout has been reached before the call number " + nbCall + ".");
				this.log.debug("InterruptedException was:", ce);
				if (harderResult.equals(Result.VALID)) {
					harderResult = Result.UNFINISHED_EXECUTION;
				}
				continue;
			}

			this.log.debug("Try validating output number " + nbCall + " (" + res.getUri() + ") .");

			final boolean validate = this.validator.validate(res);
			if (!validate) {
				this.log.error("The result number " + nbCall + " was not valid (" + res.getUri() + ").");
				if (harderResult.equals(Result.VALID) || harderResult.equals(Result.UNFINISHED_EXECUTION)) {
					harderResult = Result.INVALID;
				}
				continue;
			}

			if (this.generateOutput) {
				this.marshallOutput(res, nbCall);
			}
		}

		this.log.info("End of test validation.");
		if (this.generateOutput) {
			this.log.info("Marshalled output (if any) are in " + this.getOutputFolder() + ".");
		}

		return harderResult;
	}


	/**
	 * Write the resource in a file having the usageContext hashcode and the nb of the call in its name.
	 *
	 * @param res
	 *            The resource to marshal
	 * @param nbCall
	 *            The number of the call to be serialised
	 */
	private void marshallOutput(final Resource res, final int nbCall) {
		final File outputFile = new File(this.getOutputFolder(), nbCall + ".xml");
		final File parent = outputFile.getParentFile();
		if (!parent.exists() && !parent.mkdirs()) {
			this.log.error("Unable to marshall resource (" + res.getUri() + ") + in file " + outputFile.getAbsolutePath() + ". Parent folder can't be created.");
		}
		try {
			new WebLabMarshaller().marshalResource(res, outputFile);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn("Unable to marshall resource (" + res.getUri() + ") + in file " + outputFile.getAbsolutePath() + ".");
		}
	}


	/**
	 * @return The name of the folder where output are serialised.
	 */
	protected File getOutputFolder() {
		final String folderName;
		if (this.usageContext == null) {
			folderName = "null";
		} else {
			folderName = this.usageContext.hashCode() + "";
		}
		return new File(this.outputFolder, folderName);
	}
}
