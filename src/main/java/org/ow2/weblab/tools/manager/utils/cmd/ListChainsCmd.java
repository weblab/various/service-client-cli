/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.chain.manager.ListChainsRequest;
import org.ow2.weblab.chain.manager.ListChainsResponse;
import org.ow2.weblab.tools.manager.utils.URLDelegate;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;



/**
 * This class describes the list chains command
 *
 * @author ymombrun
 * @date 2013-04-30
 */
@Parameters(commandNames = ListChainsCmd.LIST_CHAINS, commandDescription = "List the Chains deployed on the ESB.")
public final class ListChainsCmd extends AManagerClient {


	private static final String NO_CHAIN = "Not any chain deployed on the Manager.";


	public static final String LIST_CHAINS = "listChains";


	@ParametersDelegate
	final URLDelegate urlParam = new URLDelegate();


	@Override
	public String getServiceUrl() {
		return this.urlParam.getUrl();
	}


	@Override
	protected String getSoapAction() {
		return ListChainsCmd.LIST_CHAINS;
	}


	@Override
	public String callServiceAndGetResults() {
		final ChainManager client = this.createChainManagerClient();
		this.log.debug("Calling " + this.getSoapAction() + ".");
		final ListChainsResponse resp = client.listChains(new ListChainsRequest());
		this.log.debug("Answer received for " + this.getSoapAction() + ".");

		final String message;
		if (resp == null) {
			this.log.debug("ListChainsResponse is null. Assuming that no chain is deployed.");
			message = ListChainsCmd.NO_CHAIN;
		} else {
			final List<String> chains = resp.getChainId();
			if (chains.isEmpty()) {
				message = ListChainsCmd.NO_CHAIN;
			} else {
				message = "The following chains are deployed:\n\t" + StringUtils.join(chains, "\n\t");
			}
		}
		this.log.debug(message);
		return message;
	}

}
