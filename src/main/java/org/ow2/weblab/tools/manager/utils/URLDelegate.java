/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.tools.manager.utils;

import com.beust.jcommander.Parameter;


/**
 * An holder for the URL parameter (which is common to every command). It enables to prevent from copy pasting its description in every command.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public class URLDelegate {


	@Parameter(description = "This is the URL of the Instance Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager", names = {
			"-u", "--url", "--address" }, required = true, validateValueWith = URLValidator.class)
	String url;


	public String getUrl() {
		return this.url;
	}

}
