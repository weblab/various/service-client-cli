/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import org.ow2.weblab.chain.ChainRequestType;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.chain.manager.ChainStatus;
import org.ow2.weblab.chain.manager.ChainStatusEnum;
import org.ow2.weblab.chain.manager.StartChainRequest;
import org.ow2.weblab.tools.manager.utils.ChainIdDelegate;
import org.ow2.weblab.tools.manager.utils.ChainRequestTypeConverter;
import org.ow2.weblab.tools.manager.utils.URLDelegate;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.beust.jcommander.converters.BooleanConverter;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.validators.PositiveInteger;



/**
 * This class describes the start instance command.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
@Parameters(commandNames = StartCmd.START, commandDescription = "Start an instance of the given chain Id deployed on the ESB with the given parameters.")
public final class StartCmd extends AStatusParser {


	public static final String START = "start";


	@ParametersDelegate
	final URLDelegate urlParam = new URLDelegate();


	@ParametersDelegate
	final ChainIdDelegate chainIdParam = new ChainIdDelegate();


	// Every other parameters are optional

	@Parameter(description = "Minimum delay between two chain calls (when a document is processed).", converter = IntegerConverter.class, names = "--minimumWaitingTime", required = false, validateWith = PositiveInteger.class)
	Integer minimumWaitingTime;


	@Parameter(description = "Maximum delay between two processed documents. When there is no more resource to process, the delay between two retries increase. This value represents the maximum delay to wait.", converter = IntegerConverter.class, names = "--maximumWaitingTime", required = false, validateWith = PositiveInteger.class)
	Integer maximumWaitingTime;


	@Parameter(description = "Wether or not the chain should be executed even when the queuemanager at the beginining throws EmptyQueueExceptions. If this value is false, the chain manager will stop the instance automatically if the chain reply 0-0-0. If true, the chain manager will retry from minDelay to maxDelay.", converter = BooleanConverter.class, names = "--continuous", required = false)
	Boolean continuous;


	@Parameter(description = "Each chain can be launch in parallel, if every service really loosely coupled and \"parallel safe\". If not, juste put 1 as value, if > 1 multiple chain of the same instance will be launched in parallel.", converter = IntegerConverter.class, names = {
			"--nbOfParallelsCall", "-p" }, required = false, validateWith = PositiveInteger.class)
	Integer nbOfParallelsCall;


	@Parameter(description = "The parameter that will be passed to the chain instances. It will be enriched automatically with the usage context of the fired instance.", converter = ChainRequestTypeConverter.class, names = {
			"--chainRequest", "--params" }, required = false)
	ChainRequestType chainRequest;


	@Parameter(description = "This is the identifier of the chain Instance to be created. The value here will override any value provided inside the chainRequest file.", names = {
			"-c", "--context", "--usageContext" })
	String usageContext;


	@Override
	public String getServiceUrl() {
		return this.urlParam.getUrl();
	}


	@Override
	protected String getSoapAction() {
		return StartCmd.START;
	}


	@Override
	public String callServiceAndGetResults() {
		final ChainManager client = this.createChainManagerClient();
		final StartChainRequest res = new StartChainRequest();
		res.setChainId(this.chainIdParam.getChainId());
		res.setContinuous(this.continuous);
		res.setMaximumWaitingTime(this.maximumWaitingTime);
		res.setMinimumWaitingTime(this.minimumWaitingTime);
		res.setNbOfParallelsCall(this.nbOfParallelsCall);
		if (this.usageContext != null && this.chainRequest != null && !this.usageContext.equals(this.chainRequest.getUsageContext())) {
			this.log.info("Context parameter (" + this.usageContext + ") will replace context provided in ChainRequest parameter ("
					+ this.chainRequest.getUsageContext() + ").");
			this.chainRequest.setUsageContext(this.usageContext);
		} else if (this.chainRequest != null || this.usageContext != null) {
			if (this.chainRequest == null) {
				this.chainRequest = new ChainRequestType();
			}
			if (this.usageContext != null) {
				this.chainRequest.setUsageContext(this.usageContext);
			}
		} else {
			// Both chainRequest and usageContext are null. Doing nothing
		}
		res.setChainRequest(this.chainRequest); // Even if chainRequest is null

		this.log.debug("Calling " + this.getSoapAction() + " with chainId " + this.chainIdParam.getChainId());
		final ChainStatus resp = client.start(res);
		this.log.debug("Answer received for " + this.getSoapAction() + " with chainId " + this.chainIdParam.getChainId());

		final String message;
		if (ChainStatusEnum.RUNNING.equals(resp.getStatus())) {
			message = "Instance " + resp.getChainId() + " of context " + resp.getUsageContext() + " successfully started.\n\nDetails";
			this.log.debug(message);
		} else {
			message = "Instance " + resp.getChainId() + " of context " + resp.getUsageContext()
					+ " has been created but seems to have been stopped automatically.\n\nDetails";
			this.log.error(message);
		}

		return message + this.toString(resp);
	}

}
