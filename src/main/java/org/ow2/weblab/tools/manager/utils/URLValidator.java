/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.logging.LogFactory;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.ParameterException;



/**
 * Converts a string parameter read from the command line arguments into a valid URL.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public class URLValidator implements IValueValidator<String> {


	@Override
	public void validate(final String name, final String value) throws ParameterException {
		final URL url;
		try {
			url = new URL(value);
		} catch (final MalformedURLException murle) {
			final String message = "Parameter " + name + " should be a valid URL (found " + value + ")";
			LogFactory.getLog(this.getClass()).debug(message, murle);
			throw new ParameterException(message);
		}
		if (!url.getProtocol().startsWith("http")) {
			final String message = "Parameter " + name + " should be a valid HTTP URL (found " + value + ")";
			throw new ParameterException(message);
		}
	}




}
