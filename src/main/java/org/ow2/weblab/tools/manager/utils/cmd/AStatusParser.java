/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.ow2.weblab.chain.manager.ChainStatus;


/**
 * This class is an abstract sub-class of the ManagerClient.
 * It just aims at providing a method that converts a ChainStatus to a printable String.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public abstract class AStatusParser extends AManagerClient {


	private static final String FULL_HEADER = StringUtils.join(new String[] { "chainId", "usageContext", "startDate", "lastProcessedDocumentDate", "statusStr",
			"nbDocOk", "oKMeanProcessingTime", "oKMaxProcessingTime", "oKMinProcessingTime", "nbDocError", "nbDocSkipped", "oKTotalProcessingTime",
			"errorTotalProcessingTime", "skippedTotalProcessingTime" }, "; ");


	private static final String SIMPLE_HEADER = StringUtils.join(new String[] { "chainId", "usageContext", "startDate", "lastProcessedDocumentDate",
			"statusStr", "nbDocOk", "oKMeanProcessingTime" }, "; ");



	public String toString(final ChainStatus status) {
		final String chainId = status.getChainId();
		final String usageContext = status.getUsageContext();
		final String startDate = this.toReadableTime(status.getStartDate());
		final String lastProcessedDocumentDate = this.toReadableTime(status.getLastProcessedDocumentDate());
		final String statusStr = status.getStatus().toString();
		final String nbDocOk = String.format("%d", Long.valueOf(status.getNbDocOK()));
		final String oKMeanProcessingTime;
		if (status.getNbDocOK() != 0) {
			oKMeanProcessingTime = this.toReadableTime(status.getOKTotalProcessingTime() / status.getNbDocOK());
		} else {
			oKMeanProcessingTime = "-"; // Not computable
		}

		// Only printed to logs to have an easy to read output
		final String oKMaxProcessingTime = this.toReadableTime(status.getOKMaxProcessingTime());
		final String oKMinProcessingTime = this.toReadableTime(status.getOKMinProcessingTime());
		final String nbDocError = String.format("%d", Long.valueOf(status.getNbDocError()));
		final String nbDocSkipped = String.format("%d", Long.valueOf(status.getNbDocSkipped()));
		final String oKTotalProcessingTime = this.toReadableTime(status.getOKTotalProcessingTime());
		final String errorTotalProcessingTime = this.toReadableTime(status.getErrorTotalProcessingTime());
		final String skippedTotalProcessingTime = this.toReadableTime(status.getSkippedTotalProcessingTime());

		this.log.debug(AStatusParser.FULL_HEADER
				+ "\n\t"
				+ StringUtils.join(new String[] { chainId, usageContext, startDate, lastProcessedDocumentDate, statusStr, nbDocOk, oKMeanProcessingTime,
						oKMaxProcessingTime, oKMinProcessingTime, nbDocError, nbDocSkipped, oKTotalProcessingTime, errorTotalProcessingTime,
						skippedTotalProcessingTime }, "; "));

		return AStatusParser.SIMPLE_HEADER
				+ "\n\t"
				+ StringUtils
						.join(new String[] { chainId, usageContext, startDate, lastProcessedDocumentDate, statusStr, nbDocOk, oKMeanProcessingTime }, "; ");
	}


	private synchronized String toReadableTime(final XMLGregorianCalendar startDate) {
		// Synchronised method since SimpleDateFormat is not thread safe
		if (startDate == null) {
			return "-";
		}
		final Date date = startDate.toGregorianCalendar().getTime();
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date);
	}


	private String toReadableTime(final long errorTotalProcessingTime) {
		final long hours = TimeUnit.MILLISECONDS.toHours(errorTotalProcessingTime);
		final long minutes = TimeUnit.MILLISECONDS.toMinutes(errorTotalProcessingTime) - TimeUnit.HOURS.toMinutes(hours);
		final long seconds = TimeUnit.MILLISECONDS.toSeconds(errorTotalProcessingTime) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes);
		final long millis = errorTotalProcessingTime - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes) - TimeUnit.SECONDS.toMillis(seconds);

		return String.format("%d:%02d:%02d,%03d", Long.valueOf(hours), Long.valueOf(minutes), Long.valueOf(seconds), Long.valueOf(millis));
	}

}
