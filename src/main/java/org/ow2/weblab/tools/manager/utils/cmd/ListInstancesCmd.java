/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.chain.manager.ListInstancesRequest;
import org.ow2.weblab.chain.manager.ListInstancesResponse;
import org.ow2.weblab.tools.manager.utils.ChainIdDelegate;
import org.ow2.weblab.tools.manager.utils.URLDelegate;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;


/**
 * This class describes the list instances command.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
@Parameters(commandNames = ListInstancesCmd.LIST_INSTANCES, commandDescription = "List the instances deployed on the ESB for a given chain Id.")
public final class ListInstancesCmd extends AManagerClient {


	public static final String LIST_INSTANCES = "listInstances";


	private static final String NO_INSTANCE = "Not any instance deployed on the Manager for '";


	@ParametersDelegate
	final URLDelegate urlParam = new URLDelegate();


	@ParametersDelegate
	final ChainIdDelegate chainIdParam = new ChainIdDelegate();


	@Override
	public String getServiceUrl() {
		return this.urlParam.getUrl();
	}


	@Override
	protected String getSoapAction() {
		return ListInstancesCmd.LIST_INSTANCES;
	}


	@Override
	public String callServiceAndGetResults() {
		final ChainManager client = this.createChainManagerClient();
		final ListInstancesRequest req = new ListInstancesRequest();
		final String chainId = this.chainIdParam.getChainId();
		req.setChainId(chainId);

		this.log.debug("Calling " + this.getSoapAction() + " with chain id " + chainId);
		final ListInstancesResponse resp = client.listInstances(req);
		this.log.debug("Answer received for " + this.getSoapAction() + " with chain id " + chainId);

		final String message;
		if (resp == null) {
			this.log.debug("ListChainsResponse is null. Assuming that no chain is deployed.");
			message = ListInstancesCmd.NO_INSTANCE + chainId + "'.";
		} else {
			final List<String> instances = resp.getUsageContext();
			if (instances.isEmpty()) {
				message = ListInstancesCmd.NO_INSTANCE + chainId + "'.";
			} else {
				message = "The following instances are deployed on '" + chainId + ":\n\t" + StringUtils.join(instances, "\n\t");
			}
		}
		this.log.debug(message);
		return message;
	}

}
