/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.chain.manager.ChainStatus;
import org.ow2.weblab.chain.manager.StatusChainRequest;
import org.ow2.weblab.tools.manager.utils.URLDelegate;
import org.ow2.weblab.tools.manager.utils.UsageContextDelegate;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;


/**
 * This class describes the get status command.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
@Parameters(commandNames = StatusCmd.STATUS, commandDescription = "Get the status of the instance deployed on the ESB for the given usage context.")
public final class StatusCmd extends AStatusParser {


	public static final String STATUS = "status";


	@ParametersDelegate
	final URLDelegate urlParam = new URLDelegate();


	@ParametersDelegate
	final UsageContextDelegate usageContextParam = new UsageContextDelegate();


	@Override
	public String getServiceUrl() {
		return this.urlParam.getUrl();
	}


	@Override
	protected String getSoapAction() {
		return StatusCmd.STATUS;
	}


	@Override
	public String callServiceAndGetResults() {
		final ChainManager client = this.createChainManagerClient();
		final StatusChainRequest res = new StatusChainRequest();
		res.setUsageContext(this.usageContextParam.getUsageContext());
		this.log.debug("Calling " + this.getSoapAction() + " with context " + this.usageContextParam.getUsageContext());
		final ChainStatus resp = client.status(res);
		this.log.debug("Answer received for " + this.getSoapAction() + " with context " + this.usageContextParam.getUsageContext());
		return this.toString(resp);
	}

}
