/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.chain.ChainRequestType;

import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.BaseConverter;


/**
 * Converts a String representing the path to an XML file containing a ChainRequestType into an instance of ChainRequestType.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public class ChainRequestTypeConverter extends BaseConverter<ChainRequestType> {


	private static final String A_TYPE = "a ChainRequestType";


	private final Log log;


	public ChainRequestTypeConverter(final String optionName) {
		super(optionName);
		this.log = LogFactory.getLog(this.getClass());
	}


	@Override
	public ChainRequestType convert(final String value) {
		if (value == null) {
			throw new ParameterException(this.getErrorString(value, ChainRequestTypeConverter.A_TYPE));
		}
		final File file = new File(value);
		if (!file.isFile()) {
			throw new ParameterException(this.getErrorString(value, ChainRequestTypeConverter.A_TYPE) + ". " + file.getAbsolutePath() + " does not exists.");
		}

		final JAXBContext context;
		try {
			context = JAXBContext.newInstance(ChainRequestType.class);
		} catch (final JAXBException e) {
			final String message = "Unable to create JAXBContext for " + ChainRequestTypeConverter.A_TYPE + ".";
			this.log.debug(message, e);
			throw new ParameterException(message);
		}
		final StreamSource streamSource = new StreamSource(file);
		try {
			return context.createUnmarshaller().unmarshal(streamSource, ChainRequestType.class).getValue();
		} catch (final JAXBException e) {
			final String message = "Unable to unmarshall file " + file.getAbsolutePath() + " into " + ChainRequestTypeConverter.A_TYPE + ".";
			this.log.debug(message, e);
			throw new ParameterException(message);
		}
	}


}
