/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.manager.utils.cmd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.core.extended.util.ServiceUtil;


/**
 * This class is the Abstract mother class of every ChainManager Cmd (one per action).
 * It shares the code that instantiate the service and defines the main method callServiceAndGetResults.
 *
 * @author ymombrun
 * @date 2013-04-30
 */
public abstract class AManagerClient {


	/**
	 * The logger used inside subclasses.
	 */
	protected final Log log = LogFactory.getLog(this.getClass());


	/**
	 * @return The value of the URL parameter which the only one that is common to every commands.
	 */
	protected abstract String getServiceUrl();


	/**
	 * @return The soapAction to be used when calling the remote ChainManager. It is often the command name.
	 */
	protected abstract String getSoapAction();


	/**
	 * @return An instance of ChainManager set up to remote call the URL provided by {@link #getServiceUrl()}, using the SOAP action provided by
	 *         {@link #getSoapAction()}.
	 */
	protected ChainManager createChainManagerClient() {
		final ChainManager client = new org.ow2.weblab.chain.ChainManager().getChainManagerPort();
		ServiceUtil.setEndpointAddress(client, this.getServiceUrl(), this.getSoapAction());
		return client;
	}


	/**
	 * Call the service with the parameter provided by the command and converts the results to an user friendly string to be printed to the system out.
	 *
	 * @return The Result of the service call in an user friendly format
	 */
	public abstract String callServiceAndGetResults();

}
