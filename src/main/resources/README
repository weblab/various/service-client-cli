service-client-cli contains two distinct Command Line Interface that can be used interact with the WebLab platform: weblab-batch-client and weblab-manager-client.
 

=========================== WEBLAB-BATCH-CLIENT =============================
weblab-batch-client enables to call and validate WebLab services.

It can be launched using weblab-batch-client.bat or weblab-batch-client.sh scripts plus the following parameters:

Required:
	-i <interfaceName>, where interfaceName can either be analyser or indexer. It enables to select one of the service interface for which a WebServiceCaller has been implemented.
	-u <url>, where url is the endpoint address to be called.

Optional:
	-c <usageContext>, where usageContext is the usageContext to be used for each call. Default value is the empty String.
	-t <tempo>, where tempo is the number of millisecond to wait before each call in the thread pool. Default value is 500.
	-w <timeout>, where timeout is the number of millisecond to let the executor execute every web service call. Default value is 120000. When reached everything is considered as failed.
	-p <nbThread>, where nbThread is the number or thread to call the web service in parallel. Default value is 1.
	-v <deactivateValidation>, where deactivateValidation is true or false, and defines whether or not to deactivate the validation of the I/O w.r.t. the XSD. Default value if false (i.e. validate I/O).

Required for analyser, configurable and indexer interfaces only (not used ofr other):
	-f <inputFolder>, where inputFolder is the path to a folder of XML input files to be sent to indexer/analyser.

Optional for analyser and queuemanager interfaces only (not used for other)
	-o <outputFolder>, where outputFolder is the path to a folder to be used to store results of web service calls. If null, nothing is stored.

Optional for queuemanager interface only (not used for other)
	-m <maxNbCall>, where maxNbCall is the number of times to call nextResource with success. Default value is 200.

For instance:
	weblab-client-cli -i analyser -u http://localhost:8080/test-service/analyser-endpoint -f /tmp/testdata -o /tmp/output
	
	
=========================== WEBLAB-MANAGER-CLIENT =============================
weblab-manager-client enables to call ChainManager service deployed on the ESB.

It can be launched using weblab-manager-client.bat or weblab-manager-client.sh scripts plus the following parameters:

Usage: weblab-manager-client [command] [command options]
  Commands:
  
    listChains      List the Chains deployed on the ESB.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
             
    listInstances      List the instances deployed on the ESB for a given chain Id.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -i, --chainId, --chain
             Unique identifier of the Chain in the Chain Manager component (most  of the time it is its endpoint name).

    start      Start an instance of the given chain Id deployed on the ESB with the given parameters.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -i, --chainId, --chain
             Unique identifier of the Chain in the Chain Manager component (most of the time it is its endpoint name).
          --chainRequest, --params
             The parameter that will be passed to the chain instances. It will be enriched automatically with the usage context of the fired instance.
          --continuous
             Wether or not the chain should be executed even when the queuemanager at the beginining throws EmptyQueueExceptions. If this value is false, the chain manager will stop the instance automatically if the chain reply 0-0-0. If true, the chain manager will retry from minDelay to maxDelay.
          --maximumWaitingTime
             Maximum delay between two processed documents. When there is no more resource to process, the delay between two retries increase. This value represents the maximum delay to wait.
          --minimumWaitingTime
             Minimum delay between two chain calls (when a document is processed).
          --nbOfParallelsCall, -p
             Each chain can be launch in parallel, if every service really loosely coupled and "parallel safe". If not, juste put 1 as value, if > 1 multiple chain of the same instance will be launched in parallel.
           -c, --context, --usageContext
             This is the identifier of the chain Instance to be created. The value here will override any provided inside chainRequest file.

    pause      Pauses the instance deployed on the ESB for the given usage context.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -c, --context, --usageContext
             This is the identifier of the chain Instance in the Chain Manager component.

    stop      Stops the instance deployed on the ESB for the given usage context.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -c, --context, --usageContext
             This is the identifier of the chain Instance in the Chain Manager component.

    status      Get the status of the instance deployed on the ESB for the given usage context.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -c, --context, --usageContext
             This is the identifier of the chain Instance in the Chain Manager component.

    deleteInstance      Delete the instance deployed on the ESB for the given usage context.
        * -u, --url, --address
             This is the URL of the Chain Manager to be called. Usually it is something like: http://petals:8084/petals/services/ChainManager
        * -c, --context, --usageContext
             This is the identifier of the chain Instance in the Chain Manager component.

For instance: 

weblab-manager-client.bat listChains -u http://localhost:8084/petals/services/ChainManager
weblab-manager-client.bat listInstances -u http://localhost:8084/petals/services/ChainManager -i T0
weblab-manager-client.bat start -u http://localhost:8084/petals/services/ChainManager -i T0


=========================================================================
