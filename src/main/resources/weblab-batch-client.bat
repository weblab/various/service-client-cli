@echo off
echo Starting the service client with the following java
java -version
java -Dlog4j.configuration=file:conf/log4j.properties -Dfile.encoding=utf8 -jar lib\${project.artifactId}-${project.version}.jar %*
pause
