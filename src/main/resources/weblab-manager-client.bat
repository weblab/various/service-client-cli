@echo off
echo Starting the service client with the following java
java -version
java -Dlog4j.configuration=file:conf/log4j.properties -Dfile.encoding=utf8 -cp lib/${project.artifactId}-${project.version}.jar org.ow2.weblab.tools.main.WebLabManagerClient %*
pause
