/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.services.QueueManager;



/**
 * @author ymombrun
 * @date 2012-07-12
 */
public final class CommandLineParserTest {


	private static final String NAN = "this in not a number";


	private static final String C = "-c";


	private static final String F = "-f";


	private static final String GOOD_INPUT_FOLDER = "src/test/resources";


	private static final String GOOD_INTERFACE = "analyser";


	private static final String GOOD_NB_THREAD = "2";


	private static final String GOOD_OUTPUT_FOLDER = "target/output";


	private static final String GOOD_TEMPO = "1000";


	private static final String GOOD_TIMEOUT = "60000";


	private static final String GOOD_URL = "http://localhost:8080/gate-extraction/analyser";


	private static final String GOOD_USAGE_CONTEXT = "usageContext";


	private static final String I = "-i";


	private static final String M = "-m";


	private static final String O = "-o";


	private static final String P = "-p";


	private static final String T = "-t";


	private static final String U = "-u";


	private static final String W = "-w";


	private static final String QUEUE = QueueManager.class.getSimpleName();


	private final String[] minimalArgsArray = { CommandLineParserTest.I, CommandLineParserTest.GOOD_INTERFACE, CommandLineParserTest.U,
			CommandLineParserTest.GOOD_URL };


	@Test()
	public void testAAADefaultParameters() throws ParseException {
		final String[] args = this.minimalArgsArray;
		Assert.assertNull(new WebLabBatchCmdParser(args).getOutputFolder());
		Assert.assertEquals("", new WebLabBatchCmdParser(args).getUsageContextOption());
		Assert.assertEquals("1", String.valueOf(new WebLabBatchCmdParser(args).getNbThreadOption()));
		Assert.assertEquals("500", String.valueOf(new WebLabBatchCmdParser(args).getTemporisation()));
		Assert.assertEquals("120000", String.valueOf(new WebLabBatchCmdParser(args).getTimeoutOption()));
	}


	@Test()
	public void testAAAGoodParameters() throws ParseException {
		final String[] args = this.enrichArrays(CommandLineParserTest.C, CommandLineParserTest.GOOD_USAGE_CONTEXT, CommandLineParserTest.U,
				CommandLineParserTest.GOOD_URL, CommandLineParserTest.O, CommandLineParserTest.GOOD_OUTPUT_FOLDER, CommandLineParserTest.F,
				CommandLineParserTest.GOOD_INPUT_FOLDER, CommandLineParserTest.P, CommandLineParserTest.GOOD_NB_THREAD, CommandLineParserTest.T,
				CommandLineParserTest.GOOD_TEMPO, CommandLineParserTest.W, CommandLineParserTest.GOOD_TIMEOUT);
		Assert.assertEquals(new File(CommandLineParserTest.GOOD_INPUT_FOLDER), new WebLabBatchCmdParser(args).getInputFile());
		Assert.assertEquals(CommandLineParserTest.GOOD_INTERFACE, new WebLabBatchCmdParser(args).getInterfaceName());
		Assert.assertEquals(new File(CommandLineParserTest.GOOD_OUTPUT_FOLDER), new WebLabBatchCmdParser(args).getOutputFolder());
		Assert.assertEquals(CommandLineParserTest.GOOD_URL, new WebLabBatchCmdParser(args).getUrl().toString());
		Assert.assertEquals(CommandLineParserTest.GOOD_USAGE_CONTEXT, new WebLabBatchCmdParser(args).getUsageContextOption());
		Assert.assertEquals(CommandLineParserTest.GOOD_NB_THREAD, String.valueOf(new WebLabBatchCmdParser(args).getNbThreadOption()));
		Assert.assertEquals(CommandLineParserTest.GOOD_TEMPO, String.valueOf(new WebLabBatchCmdParser(args).getTemporisation()));
		Assert.assertEquals(CommandLineParserTest.GOOD_TIMEOUT, String.valueOf(new WebLabBatchCmdParser(args).getTimeoutOption()));
	}


	@Test(expected = ParseException.class)
	public void testInputFolderDoesNotExists() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.F, "dsodj/kojdks/jhjs")).getInputFile();
	}


	@Test(expected = ParseException.class)
	public void testInputFolderMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.F)).getInputFile();
	}


	@Test(expected = ParseException.class)
	public void testInputFolderMissingRequired() throws ParseException {
		new WebLabBatchCmdParser(this.minimalArgsArray).getInputFile();
	}


	@Test(expected = ParseException.class)
	public void testInterfaceMissingParameterRequired() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.U, CommandLineParserTest.GOOD_URL };
		new WebLabBatchCmdParser(args).getInterfaceName();
	}


	@Test(expected = ParseException.class)
	public void testInterfaceMissingRequired() throws ParseException {
		final String[] args = { CommandLineParserTest.U, CommandLineParserTest.GOOD_URL };
		new WebLabBatchCmdParser(args).getInterfaceName();
	}


	@Test(expected = ParseException.class)
	public void testNbThreadsMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.P)).getNbThreadOption();
	}


	@Test(expected = ParseException.class)
	public void testNbThreadsNaN() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.P, NAN)).getNbThreadOption();
	}


	@Test(expected = ParseException.class)
	public void testNbThreadsNegative() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.P, "-1")).getNbThreadOption();
	}


	@Test(expected = ParseException.class)
	public void testOutputFolderIsNotADirectory() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.O, "pom.xml")).getOutputFolder();
	}


	@Test(expected = ParseException.class)
	public void testOutputFolderMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.O)).getOutputFolder();
	}


	@Test(expected = ParseException.class)
	public void testMaxNbCallMissingParameterRequired() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.QUEUE, CommandLineParserTest.U, CommandLineParserTest.GOOD_URL,
				CommandLineParserTest.M };
		new WebLabBatchCmdParser(args).getMaxNbCallOption();
	}


	@Test(expected = ParseException.class)
	public void testMaxNbCallNaN() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.QUEUE, CommandLineParserTest.U, CommandLineParserTest.GOOD_URL,
				CommandLineParserTest.M, CommandLineParserTest.NAN };
		new WebLabBatchCmdParser(args).getMaxNbCallOption();
	}


	@Test(expected = ParseException.class)
	public void testMaxNbCallNegative() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.QUEUE, CommandLineParserTest.U, CommandLineParserTest.GOOD_URL,
				CommandLineParserTest.M, "-1" };
		new WebLabBatchCmdParser(args).getMaxNbCallOption();
	}



	@Test(expected = ParseException.class)
	public void testTempoMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.T)).getTemporisation();
	}


	@Test(expected = ParseException.class)
	public void testTempoNaN() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.T, "this is not a number")).getTemporisation();
	}


	@Test(expected = ParseException.class)
	public void testTempoNegative() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.T, "-1")).getTemporisation();
	}



	@Test(expected = ParseException.class)
	public void testTimeoutMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.W)).getTimeoutOption();
	}


	@Test(expected = ParseException.class)
	public void testTimeoutNaN() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.W, NAN)).getTimeoutOption();
	}


	@Test(expected = ParseException.class)
	public void testTimeoutNegative() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.W, "-1")).getTimeoutOption();
	}


	@Test(expected = ParseException.class)
	public void testUrlBad() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.GOOD_INTERFACE, CommandLineParserTest.U, "this is not an url" };
		new WebLabBatchCmdParser(args).getUrl();
	}


	@Test(expected = ParseException.class)
	public void testUrlMissingParameterRequired() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.GOOD_INTERFACE, CommandLineParserTest.U };
		new WebLabBatchCmdParser(args).getUrl();
	}


	@Test(expected = ParseException.class)
	public void testUrlMissingRequired() throws ParseException {
		final String[] args = { CommandLineParserTest.I, CommandLineParserTest.GOOD_INTERFACE };
		new WebLabBatchCmdParser(args).getUrl();
	}


	@Test(expected = ParseException.class)
	public void testUsageContextMissingParameterRequired() throws ParseException {
		new WebLabBatchCmdParser(this.enrichArrays(CommandLineParserTest.C)).getUsageContextOption();
	}


	private String[] enrichArrays(final String... args) {
		final List<String> params = new ArrayList<>(Arrays.asList(this.minimalArgsArray));
		params.addAll(Arrays.asList(args));
		return params.toArray(new String[0]);
	}

}
