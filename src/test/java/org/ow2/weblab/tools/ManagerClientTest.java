/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import java.io.File;

import javax.xml.ws.Endpoint;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.tools.main.WebLabManagerClient;
import org.ow2.weblab.tools.mocks.ChainManagerMock;



public class ManagerClientTest {


	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();


	@Rule
	public final StandardOutputStreamLog stdout = new StandardOutputStreamLog();


	private static Endpoint endpoint;


	private static int port;


	private static String address;


	private static ChainManager manager = new ChainManagerMock();


	@BeforeClass
	public static void beforeClass() {
		port = PortFinder.findFreePort();
		address = "http://localhost:" + port + "/managerMock";
		endpoint = Endpoint.publish(address, manager);
	}


	@AfterClass
	public static void afterClass() {
		if (endpoint != null)
			endpoint.stop();
	}


	@Test
	public void listChains() {
		this.exit.expectSystemExitWithStatus(0);
		this.exit.checkAssertionAfterwards(new Assertion() {


			@Override
			public void checkAssertion() throws Exception {
				Assert.assertTrue(ManagerClientTest.this.stdout.getLog().contains("T0"));
				Assert.assertTrue(ManagerClientTest.this.stdout.getLog().contains("T1"));
				Assert.assertFalse(ManagerClientTest.this.stdout.getLog().contains("T2"));
				FileUtils.write(new File("target/listChains.out"), ManagerClientTest.this.stdout.getLog());
			}
		});

		WebLabManagerClient.main(new String[] { "listChains", "-u", address });
	}


	@Test
	public void listInstancesChains() {
		this.exit.expectSystemExitWithStatus(0);
		this.exit.checkAssertionAfterwards(new Assertion() {


			@Override
			public void checkAssertion() throws Exception {
				FileUtils.write(new File("target/listInstances.out"), ManagerClientTest.this.stdout.getLog());
				final String out = ManagerClientTest.this.stdout.getLog().trim();
				Assert.assertTrue(out.contains("T0"));
				Assert.assertFalse(out.contains("T1"));
				Assert.assertFalse(out.contains("T2"));
				Assert.assertTrue(out.contains("RUNNING"));
				Assert.assertFalse(out.contains("PAUSED"));
				Assert.assertFalse(out.contains("STOPPED"));
			}
		});

		WebLabManagerClient.main(new String[] { "start", "-u", address, "-i", "T0" });
	}



}
