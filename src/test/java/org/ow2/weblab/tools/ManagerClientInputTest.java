/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 *Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import javax.xml.ws.Endpoint;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.tools.main.WebLabManagerClient;
import org.ow2.weblab.tools.mocks.ChainManagerMock;


/**
 * Checks that the various parameters given to the command line are taken into account.
 *
 * @author ymombrun
 * @date 2013-05-13
 */
public class ManagerClientInputTest {


	private static Endpoint endpoint;


	private static int port;


	private static String address;


	private static ChainManager manager = new ChainManagerMock();


	@BeforeClass
	public static void beforeClass() {
		port = PortFinder.findFreePort();
		address = "http://localhost:" + port + "/managerMock";
		endpoint = Endpoint.publish(address, manager);
	}


	@AfterClass
	public static void afterClass() {
		if (endpoint != null)
			endpoint.stop();
	}


	@Test
	public void testStartNoParam() {
		final String contextContains = "Generated";
		final String[] command = new String[] { "start", "-u", address, "-i", "T0" };
		startAndCheckUC(contextContains, command);
	}


	@Test
	public void testStartContext() {
		final String contextContains = "MyNewContext";
		final String[] command = new String[] { "start", "-u", address, "-i", "T0", "-c", "MyNewContext" };
		startAndCheckUC(contextContains, command);
	}


	@Test
	public void testStartChainRequest() {
		final String contextContains = "SampleChainRequestContext-ToBeChanged";
		final String[] command = new String[] { "start", "-u", address, "-i", "T0", "--chainRequest", "src/main/resources/SampleChainRequest.xml" };
		startAndCheckUC(contextContains, command);
	}


	@Test
	public void testStartContextAndChainRequest() {
		final String contextContains = "MyNewContext";
		final String[] command = new String[] { "start", "-u", address, "-i", "T0", "--context", "MyNewContext", "--chainRequest",
				"src/main/resources/SampleChainRequest.xml" };
		startAndCheckUC(contextContains, command);
	}


	private void startAndCheckUC(final String contextContains, final String[] command) {
		final WebLabManagerClient starter = new WebLabManagerClient(command);
		final String startMsg = starter.run();
		Assert.assertTrue(startMsg.contains("T0"));
		Assert.assertFalse(startMsg.contains("T1"));
		Assert.assertFalse(startMsg.contains("T2"));
		Assert.assertTrue(startMsg.contains("RUNNING"));
		Assert.assertFalse(startMsg.contains("PAUSED"));
		Assert.assertFalse(startMsg.contains("STOPPED"));
		Assert.assertTrue(startMsg.contains(contextContains));
	}

}
