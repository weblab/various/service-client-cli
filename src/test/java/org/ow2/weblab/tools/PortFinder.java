/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

import org.apache.commons.io.IOUtils;

/**
 * {@link "http://fahdshariff.blogspot.fr/2012/10/java-find-available-port-number.html"}
 *
 *
 *
 * Finds an available port on localhost.
 */
public class PortFinder {


	// the ports below 1024 are system ports
	private static final int MIN_PORT_NUMBER = 1024;


	// the ports above 49151 are dynamic and/or private
	private static final int MAX_PORT_NUMBER = 49151;


	/**
	 * Finds a free port between 1024 and 49151.
	 *
	 * @return a free port
	 * @throw RuntimeException if a port could not be found
	 */
	public static int findFreePort() {
		for (int i = PortFinder.MIN_PORT_NUMBER; i <= PortFinder.MAX_PORT_NUMBER; i++) {
			if (PortFinder.available(i)) {
				return i;
			}
		}
		throw new RuntimeException("Could not find an available port between " + PortFinder.MIN_PORT_NUMBER + " and " + PortFinder.MAX_PORT_NUMBER);
	}


	/**
	 * Returns true if the specified port is available on this host.
	 *
	 * @param port
	 *            the port to check
	 * @return true if the port is available, false otherwise
	 */
	private static boolean available(final int port) {
		final ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			try {
				serverSocket.setReuseAddress(true);
				try (final DatagramSocket dataSocket = new DatagramSocket(port);){
					dataSocket.setReuseAddress(true);
					return true;
				}
			} finally {
				IOUtils.closeQuietly(serverSocket);
			}
		} catch (final IOException ioe) {
			return false;
		}
	}
}