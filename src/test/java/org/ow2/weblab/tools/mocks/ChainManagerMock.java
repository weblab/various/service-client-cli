/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.mocks;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.ow2.weblab.chain.ChainRequestType;
import org.ow2.weblab.chain.manager.ChainManager;
import org.ow2.weblab.chain.manager.ChainStatus;
import org.ow2.weblab.chain.manager.ChainStatusEnum;
import org.ow2.weblab.chain.manager.DeleteInstanceRequest;
import org.ow2.weblab.chain.manager.DeleteInstanceResponse;
import org.ow2.weblab.chain.manager.ListChainsRequest;
import org.ow2.weblab.chain.manager.ListChainsResponse;
import org.ow2.weblab.chain.manager.ListInstancesRequest;
import org.ow2.weblab.chain.manager.ListInstancesResponse;
import org.ow2.weblab.chain.manager.PauseChainRequest;
import org.ow2.weblab.chain.manager.StartChainRequest;
import org.ow2.weblab.chain.manager.StatusChainRequest;
import org.ow2.weblab.chain.manager.StopChainRequest;

/**
 * Just a simple class to be used for testing the client
 *
 * @author ymombrun
 * @date 2013-05-03
 */
@WebService(endpointInterface = "org.ow2.weblab.chain.manager.ChainManager")
public class ChainManagerMock implements ChainManager {


	private static final Map<String, Set<Instance>> instancesByChainId = Collections.synchronizedMap(new HashMap<String, Set<Instance>>());


	private static final Map<String, Instance> instancesByContext = new HashMap<>();


	public ChainManagerMock() {
		if (ChainManagerMock.instancesByChainId.isEmpty()) {
			ChainManagerMock.instancesByChainId.put("T0", new HashSet<Instance>());
			ChainManagerMock.instancesByChainId.put("T1", new HashSet<Instance>());

			final StartChainRequest scr = new StartChainRequest();
			scr.setChainId("T0");
			this.start(scr);
		}
	}


	@Override
	public ListInstancesResponse listInstances(final ListInstancesRequest payload) {
		final String chainId = payload.getChainId();
		final Set<Instance> instances = ChainManagerMock.instancesByChainId.get(chainId);
		final ListInstancesResponse resp = new ListInstancesResponse();
		if (instances != null) {
			for (final Instance inst : instances) {
				inst.touch();
				resp.getUsageContext().add(inst.context);
			}
		}
		return resp;
	}


	@Override
	public DeleteInstanceResponse deleteInstance(final DeleteInstanceRequest payload) {
		final String context = payload.getUsageContext();
		if (context != null) {
			final Instance toBeRemoved = ChainManagerMock.instancesByContext.remove(context);
			if (toBeRemoved != null) {
				ChainManagerMock.instancesByChainId.get(toBeRemoved.chainId).remove(toBeRemoved);
			}
		}
		return new DeleteInstanceResponse();
	}


	@Override
	public ChainStatus status(final StatusChainRequest payload) {
		final String context = payload.getUsageContext();
		final Instance inst = ChainManagerMock.instancesByContext.get(context);
		inst.touch();
		return inst.toChainStatus();
	}


	@Override
	public ChainStatus pause(final PauseChainRequest payload) {
		final String context = payload.getUsageContext();
		final Instance inst = ChainManagerMock.instancesByContext.get(context);
		inst.pause();
		inst.touch();
		return inst.toChainStatus();
	}


	@Override
	public ChainStatus start(final StartChainRequest payload) {
		final String chainId = payload.getChainId();
		final ChainRequestType crt = payload.getChainRequest();
		final Instance inst;
		if (crt != null && crt.getUsageContext() != null) {
			inst = new Instance(chainId, crt.getUsageContext());
		} else {
			inst = new Instance(chainId);
		}
		instancesByChainId.get(chainId).add(inst);
		instancesByContext.put(inst.context, inst);
		return inst.toChainStatus();
	}


	@Override
	public ListChainsResponse listChains(final ListChainsRequest payload) {
		final ListChainsResponse resp = new ListChainsResponse();
		for (final String chainId : instancesByChainId.keySet()) {
			resp.getChainId().add(chainId);
		}
		return resp;
	}


	@Override
	public ChainStatus stop(final StopChainRequest payload) {
		final String context = payload.getUsageContext();
		final Instance inst = ChainManagerMock.instancesByContext.get(context);
		inst.stop();
		inst.touch();
		return inst.toChainStatus();
	}


	class Instance {


		public final String context;


		public final String chainId;


		public ChainStatusEnum status;


		public final XMLGregorianCalendar start;


		public XMLGregorianCalendar lastProcessedDocument;


		public Instance(final String chainId, final String context) {
			this.chainId = chainId;
			this.context = context;
			this.start = xmlCalendarNow();
			this.status = ChainStatusEnum.RUNNING;
			this.touch();
		}


		public Instance(final String chainId) {
			this(chainId, "Generated-" + UUID.randomUUID().toString());
		}


		public ChainStatus toChainStatus() {
			final ChainStatus chainStatus = new ChainStatus();
			chainStatus.setChainId(this.chainId);
			chainStatus.setLastProcessedDocumentDate(this.lastProcessedDocument);
			chainStatus.setStartDate(this.start);
			chainStatus.setStatus(this.status);
			chainStatus.setUsageContext(this.context);
			return chainStatus;
		}


		public void touch() {
			if (this.status.equals(ChainStatusEnum.RUNNING)) {
				this.lastProcessedDocument = ChainManagerMock.xmlCalendarNow();
			}
		}


		public void pause() {
			this.status = ChainStatusEnum.PAUSED;
		}


		public void stop() {
			this.status = ChainStatusEnum.STOPPED;
		}
	}


	static XMLGregorianCalendar xmlCalendarNow() {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
		} catch (final DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

}
