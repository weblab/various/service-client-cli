/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.mocks;

import java.net.URI;

import javax.jws.WebService;

import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;

/**
 * Just a simple class to be used for testing the client
 *
 * @author ymombrun
 * @date 2013-05-03
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class AnalyserMock implements Analyser {


	@Override
	public ProcessReturn process(ProcessArgs args) throws InvalidParameterException, ContentNotAvailableException, UnexpectedException, AccessDeniedException,
			UnsupportedRequestException, InsufficientResourcesException, ServiceNotConfiguredException {

		if (args == null) {
			throw ExceptionFactory.createInvalidParameterException("Args was null.");
		}

		ExceptionThrower.throwException(args.getUsageContext());

		final ProcessReturn pr = new ProcessReturn();

		if (!args.getUsageContext().isEmpty()) {
			pr.setResource(args.getResource());
			PieceOfKnowledge pok = WebLabResourceFactory.createAndLinkAnnotation(args.getResource());
			if (args.getUsageContext().equalsIgnoreCase("Invalid")) {
				pok.setData("fail!");
			} else {
				WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(pok.getUri()), pok);
				wpa.writeCanBeIgnored(Boolean.TRUE);
			}
		}
		return pr;
	}
}
