/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools.mocks;

import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;



/**
 * Just a simple class that switch over a context and throw required exception if needed
 *
 * @author ymombrun
 * @date 2013-05-03
 */
public final class ExceptionThrower {



	public static void throwException(final String context) throws ContentNotAvailableException, AccessDeniedException, InsufficientResourcesException,
			InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		if (context == null) {
			throw new NullPointerException("Context was null.");
		}
		if (context.equals(AccessDeniedException.class.getSimpleName())) {
			throw ExceptionFactory.createAccessDeniedException(context);
		}
		if (context.equals(ContentNotAvailableException.class.getSimpleName())) {
			throw ExceptionFactory.createContentNotAvailableException(context);
		}
		if (context.equals(InsufficientResourcesException.class.getSimpleName())) {
			throw ExceptionFactory.createInsufficientResourcesException(context);
		}
		if (context.equals(InvalidParameterException.class.getSimpleName())) {
			throw ExceptionFactory.createInvalidParameterException(context);
		}
		if (context.equals(ServiceNotConfiguredException.class.getSimpleName())) {
			throw ExceptionFactory.createServiceNotConfiguredException(context);
		}
		if (context.equals(UnexpectedException.class.getSimpleName())) {
			throw ExceptionFactory.createUnexpectedException(context);
		}
		if (context.equals(UnsupportedRequestException.class.getSimpleName())) {
			throw ExceptionFactory.createUnsupportedRequestException(context);
		}
	}


}
