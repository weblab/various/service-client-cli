/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.tools;

import java.io.File;
import java.io.FilenameFilter;

import javax.xml.ws.Endpoint;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.tools.main.WebLabBatchClient;
import org.ow2.weblab.tools.mocks.AnalyserMock;



/**
 * @author ymombrun
 * @date 2013-05-03
 */
public class BatchClientTest {


	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();


	private static Endpoint endpoint;


	private static int port;


	private static String address;


	private static Analyser analyser = new AnalyserMock();


	@BeforeClass
	public static void beforeClass() {
		port = PortFinder.findFreePort();
		address = "http://localhost:" + port + "/analyserMock";
		endpoint = Endpoint.publish(address, analyser);
	}


	@AfterClass
	public static void afterClass() {
		endpoint.stop();
	}


	@Test
	public void testBatchClientAnalyser() {
		final String outputFolder = "target/testBatchClientAnalyser";
		final String inputFolder = "src/test/resources/analyser";
		this.exit.expectSystemExitWithStatus(0);
		this.exit.checkAssertionAfterwards(new Assertion() {


			@Override
			public void checkAssertion() throws Exception {
				// Check that we have the same number of files in the output directory that in the input directory
				Assert.assertTrue(new File(outputFolder).exists());
				Assert.assertEquals(
						new File(inputFolder).listFiles((FilenameFilter) FileFilterUtils.makeSVNAware(FileFilterUtils.suffixFileFilter("xml"))).length,
						new File(outputFolder).listFiles().length);
			}
		});

		WebLabBatchClient.main(new String[] { "-i", "analyser", "-u", address, "-f", inputFolder, "-o", outputFolder, "-c", "ok" });
	}


	@Test
	public void testBatchClientAnalyserError() {
		final String outputFolder = "target/testBatchClientAnalyserError";
		final String inputFolder = "src/test/resources/analyser";
		this.exit.expectSystemExitWithStatus(1);
		WebLabBatchClient.main(new String[] { "-i", "analyser", "-u", address, "-f", inputFolder, "-o", outputFolder, "-c", "Invalid" });
	}
}
