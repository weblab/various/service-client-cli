# WebLab Client CLI

This project is a simple CommandLine Interface (CLI) tool enabling to call and test WebLab webservices.


# Build status
## Master
[![build status](https://gitlab.ow2.org/weblab/various/service-client-cli/badges/master/build.svg)](https://gitlab.ow2.org/weblab/various/service-client-cli/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/various/service-client-cli/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/various/service-client-cli/commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 

